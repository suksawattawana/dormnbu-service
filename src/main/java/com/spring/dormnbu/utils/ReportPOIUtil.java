package com.spring.dormnbu.utils;

import java.io.OutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReportPOIUtil {

	/* KM292 ส่วนบน */
	private static String fontReport = "TH Sarabun New";
	
	/*
	 *  Header Style
	 */
	public static XSSFCellStyle createHeaderStyle(XSSFWorkbook workbook) throws Exception {
		XSSFCellStyle style;

		try {
			style = workbook.createCellStyle();
			style.setFont(createFont(workbook, 16, true, false, 0));
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		} catch (Exception e) {
			throw e;
		}

		return style;
	}
	
	/*
	 *  Criteria Label
	 */
	public static XSSFCellStyle createCriteriaLabelStyle(XSSFWorkbook workbook) throws Exception {
		XSSFCellStyle style;

		try {
			style = workbook.createCellStyle();
			style.setFont(createFont(workbook, 14, true, false, 0));
			style.setAlignment(HorizontalAlignment.RIGHT);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		} catch (Exception e) {
			throw e;
		}

		return style;
	}
	
	/*
	 *  Criteria Value
	 */
	public static XSSFCellStyle createCriteriaValueStyle(XSSFWorkbook workbook) throws Exception {
		XSSFCellStyle style;

		try {
			style = workbook.createCellStyle();
			style.setFont(createFont(workbook, 14, false, false, 0));
			style.setAlignment(HorizontalAlignment.LEFT);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		} catch (Exception e) {
			throw e;
		}

		return style;
	}
	
	/*
	 *  Value Style
	 */
	public static XSSFCellStyle createValueStyle(XSSFWorkbook workbook) throws Exception {
		XSSFCellStyle style;

		try {
			style = workbook.createCellStyle();
			style.setFont(createFont(workbook, 14, true, false, 0));
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
		} catch (Exception e) {
			throw e;
		}

		return style;
	}
	
	/*
	 *  Font Style
	 */
	private static XSSFFont createFont(XSSFWorkbook workbook, int fontSize, boolean bold, boolean italic, int underline ) throws Exception{
		XSSFFont font = workbook.createFont(); 
		try {
			font.setFontHeightInPoints((short) fontSize);
			font.setFontName(fontReport);
			font.setBold(bold);
			font.setItalic(italic);
			font.setUnderline((byte) underline);
		} catch (Exception e) {
			throw e;
		}
		return font;
	}
	
	public static void exportExcelFile(XSSFWorkbook workbook, HttpServletResponse response, String fileName) throws Exception {
		try {
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName+".xlsx");
			OutputStream out = response.getOutputStream();
			workbook.write(out);
			out.close();
		} catch (Exception e) {
			throw e;
		}
	}
}
