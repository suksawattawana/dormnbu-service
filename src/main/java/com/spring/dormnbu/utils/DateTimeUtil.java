package com.spring.dormnbu.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {
	
	private static DateFormat formatDateS = new SimpleDateFormat("dd/MM/yyyy");
//	private static DateFormat formatDateD = new SimpleDateFormat("yyyy-MM-dd");
	private static DateFormat formatDateD_M = new SimpleDateFormat("MM/yyyy");
	

//	String to Date ---------------------------------------------------------------------------
	public static Date stringToDate(String value){
		Date result = null;
		
		try {
			if (value != null) {
				result = formatDateS.parse(value);
			}
		} catch (Exception e) {
		}
		
		return result;
	}
	
//	Date to String ---------------------------------------------------------------------------
	public static String dateToString(Date value){
		String result = null;
		
		try {
			if (value != null) {
				result = formatDateS.format(value);
			}
		} catch (Exception e) {
		}
		
		return result;
	}
	
//	CurenDate -------------------------------------------------------------------------------
	
	// String
	public static String getStringCurentDate(){
		String result = null;
		
		try {
			result = formatDateS.format(Calendar.getInstance().getTime());
			
		} catch (Exception e) {
		}
		
		return result;
	}
	
	// Date
	public static Date getDateCurentDate(){
		Date result = null;
		
		try {
			result = Calendar.getInstance().getTime();
			
		} catch (Exception e) {
		}
		
		return result;
	}
	
	public static Date getDateCurrentMonth() throws Exception{
		Date result = null;
		
		try {
			String month = formatDateD_M.format(Calendar.getInstance().getTime());
			
			month = "01/"+month;
			result = formatDateS.parse(month);
			
		} catch (Exception e) {
		}
		
		return result;
	}
	
	
	
	
}
