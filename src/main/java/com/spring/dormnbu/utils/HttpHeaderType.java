package com.spring.dormnbu.utils;

import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;

public class HttpHeaderType {

	public static HttpHeaders getHttpHeaderPDF(String pdfName) {
		HttpHeaders header = new HttpHeaders();
		header.setCacheControl(CacheControl.noCache().getHeaderValue());
		header.add("Content-Type", "application/pdf; charset=UTF-8");
		header.add("Content-Disposition", "inline; filename=\"" + pdfName + ".pdf\"");

		return header;
	}
}
