package com.spring.dormnbu.apis.review;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.review.domain.Review;
import com.spring.dormnbu.cores.review.interfaces.ReviewManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class ReviewController extends CommonController {

	@Autowired
	private ReviewManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Add
	@PostMapping("/review")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@RequestBody Review value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Review result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
	
	// Edit
	@PutMapping("/review")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse edit(@RequestBody Review value) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Review result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.edit(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
		
	// Delete
	@DeleteMapping("/review/{reviewId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse delete(@PathVariable int reviewId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.delete(reviewId);
			commonResponse = manageResult(null, ActionType.DELETE);
			
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
		
	// Search
	@GetMapping("/review/{reviewId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@PathVariable int reviewId) throws Exception{
			
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Review result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findById(reviewId);
			commonResponse = manageResult(result, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}

}