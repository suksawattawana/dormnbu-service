package com.spring.dormnbu.apis.compare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.compare.domian.Compare;
import com.spring.dormnbu.cores.compare.domian.DormResult;
import com.spring.dormnbu.cores.compare.interfaces.CompareManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class CompareController extends CommonController {

	@Autowired
	private CompareManager manager;

	@Override
	public void postConstruct() {
		
	}
	
	// Search
	@GetMapping("/compare")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@RequestBody Compare compare) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<DormResult> result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.compareDorm(compare);
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
}
