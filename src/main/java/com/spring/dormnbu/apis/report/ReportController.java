package com.spring.dormnbu.apis.report;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.report.domain.ExportPDF;
import com.spring.dormnbu.cores.report.interfaces.ReportManager;
import com.spring.dormnbu.enums.ActionType;
import com.spring.dormnbu.utils.HttpHeaderType;
import com.spring.dormnbu.utils.ReportPOIUtil;

@RestController
@RequestMapping("/api")
@Scope("request")
public class ReportController extends CommonController{

	@Autowired
	private ReportManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Search
	@GetMapping("/report/exportListDormExel")
	@Transactional(propagation = Propagation.REQUIRED)
	public void exportListDormExel(DormCriteria criteria, HttpServletResponse response) throws Exception{
		
		//1. create object
		XSSFWorkbook workbook = null;
		
		try{
			//2. ค้นหา และจัดการผลลัพธ์
			workbook = manager.exportListDormExel(criteria);
			
			//3. ถ้าไม่เป็นค่าว่าง ออกรายงาน
			if (workbook != null){
				//
				ReportPOIUtil.exportExcelFile(workbook, response, "exportListDormExel");
				System.out.println("Export Excel File Success...");
			}
			
		}catch (Exception e) {
			manageException(null, ActionType.SEARCH, e);
		}
	}
	
	// Search
	@GetMapping("/report/exportDormById/{dormId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public ResponseEntity<byte[]> exportDormById(@PathVariable int dormId, HttpServletResponse response) throws Exception{
		
		//1. create object
		ResponseEntity<byte[]> commonResponse = null;
		ExportPDF result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.exportDormById(dormId);
			
			//3. จัดการผลลัพธ์
			HttpHeaders headers = HttpHeaderType.getHttpHeaderPDF(result.getPdfName());
			commonResponse = new ResponseEntity<byte[]>(result.getPdfFile(), headers, HttpStatus.OK);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return commonResponse;
	}
	
}
