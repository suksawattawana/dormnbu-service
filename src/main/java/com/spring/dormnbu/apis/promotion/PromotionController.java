package com.spring.dormnbu.apis.promotion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class PromotionController extends CommonController {

	@Autowired
	private PromotionManager manager;
	
	@Override
	public void postConstruct() {
	}
	// Add
		@PostMapping("/promotion")
		@Transactional(propagation = Propagation.REQUIRED)
		public CommonResponse add(@Validated @RequestBody Promotion value, Errors errors) throws Exception{
			
							//1. create object
						CommonResponse commonResponse = new CommonResponse();
						Promotion result = null;
			
						try{
							//ค้นหา และจัดการผลลัพธ์
							result = manager.add(value);
							commonResponse = manageResult(result, ActionType.ADD);
									
						}catch (Exception e) {
							commonResponse = manageException(result, ActionType.ADD, e);
						}
						return commonResponse;
					}
		
						// Edit
					@PutMapping("/promotion")
					@Transactional(propagation = Propagation.REQUIRED)
					public CommonResponse edit(@RequestBody Promotion value) throws Exception{
						
						//1. create object
						CommonResponse commonResponse = new CommonResponse();
						Promotion result = null;
								
								try{
							//2.ค้นหา และจัดการผลลัพธ์
						result = manager.edit(value);
						commonResponse = manageResult(result, ActionType.EDIT);
											
						}catch (Exception e) {
							commonResponse = manageException(result, ActionType.EDIT, e);
						}
						return commonResponse;
						}
					
					// Delete
					@DeleteMapping("/promotion/{promotionId}")
					@Transactional(propagation = Propagation.REQUIRED)
					public CommonResponse delete(@PathVariable int promotionId) throws Exception{
							
						//1. create object
						CommonResponse commonResponse = new CommonResponse();
						
						try{
							//2.ค้นหา และจัดการผลลัพธ์
							manager.delete(promotionId);
							commonResponse = manageResult(null, ActionType.DELETE);
										
						}catch (Exception e) {
							commonResponse = manageException(null, ActionType.DELETE, e);
						}
						return commonResponse;
					}
}
