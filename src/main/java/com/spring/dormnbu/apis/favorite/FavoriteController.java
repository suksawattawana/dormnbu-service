package com.spring.dormnbu.apis.favorite;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class FavoriteController extends CommonController {

	@Autowired
	private FavoriteManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Add
	@PostMapping("/favorite")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@Validated @RequestBody Favorite value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Favorite result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
	
	// Delete
	@DeleteMapping("/favorite/{favoriteId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse delete(@PathVariable int favoriteId) throws Exception{
			
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.delete(favoriteId);
			commonResponse = manageResult(null, ActionType.DELETE);
						
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/favorite/{memberId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@PathVariable int id) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Favorite result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findById(id);
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
			e.printStackTrace();
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/favorite")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findByCriteria(@RequestBody FavoriteCriteria criteria) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<Favorite> result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findByCriteria(criteria);
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
			e.printStackTrace();
		}
		return commonResponse;
	}
	
}
