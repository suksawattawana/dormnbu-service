package com.spring.dormnbu.apis.dorm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.dorm.domain.Amenitie;
import com.spring.dormnbu.cores.dorm.domain.Area;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Picture;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.dorm.interfaces.DormManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class DormController extends CommonController {

	@Autowired
	private DormManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Add - dorm
	@PostMapping("/dorm")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@Validated @RequestBody Dorm value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Dorm result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
	
	// Add - roomtype
	@PostMapping("/dorm/roomtype")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse addRoomtype(@Validated @RequestBody Roomtype value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Roomtype result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.addRoomtype(value);
			commonResponse = manageResult(result, ActionType.ADD);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
	
	// edit - dorm
	@PutMapping("/dorm")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse edit(@Validated @RequestBody Dorm value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Dorm result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.edit(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// edit - amenitie
	@PutMapping("/dorm/amenitie")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse editAmenitie(@Validated @RequestBody Amenitie value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Amenitie result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.editAmenitie(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// edit - area
	@PutMapping("/dorm/area")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse editArea(@Validated @RequestBody Area value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Area result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.editArea(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// edit - picture
	@PutMapping("/dorm/picture")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse editPicture(@Validated @RequestBody Picture value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Picture result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.editPicture(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// edit - promotion
	@PutMapping("/dorm/promotion")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse editPromotion(@Validated @RequestBody Promotion value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Promotion result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.editPromotion(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// edit - roomtype
	@PutMapping("/dorm/roomtype")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse editRoomtype(@Validated @RequestBody Roomtype value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Roomtype result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.editRoomtype(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
		
	// delete
	@DeleteMapping("/dorm/{dormId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse delete(@PathVariable int dormId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.delete(dormId);
			commonResponse = manageResult(null, ActionType.DELETE);
			
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
	
	// delete - roomtype
	@DeleteMapping("/dorm/roomtype/{roomtypeId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse deleteRoomtype(@PathVariable int roomtypeId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.deleteRoomtype(roomtypeId);
			commonResponse = manageResult(null, ActionType.DELETE);
			
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/dorm/{dormId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@PathVariable int dormId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Dorm result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findById(dormId);
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/dorm/queryLastCreate")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse queryLastCreate() throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<DormLastCreate> result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.queryLastCreate();
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/dorm/queryPopular")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse queryPopular() throws Exception{
			
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<DormPopular> result = null;
			
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.queryPopular();
			commonResponse = manageResult(result, ActionType.SEARCH);
				
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}

}
