package com.spring.dormnbu.apis.comment;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.comment.domain.Comment;
import com.spring.dormnbu.cores.comment.interfaces.CommentManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class CommentController extends CommonController{
	
	@Autowired
	private CommentManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Add
	@PostMapping("/comment")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@RequestBody Comment value, Errors errors) throws Exception{
			
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Comment result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
		
	// Edit
	@PutMapping("/comment")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse edit(@RequestBody Comment value) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Comment result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.edit(value);
			commonResponse = manageResult(result, ActionType.EDIT);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
			
	// Delete
	@DeleteMapping("/comment/{commentId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse delete(@PathVariable int commentId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.delete(commentId);
			commonResponse = manageResult(null, ActionType.DELETE);
			
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
			
	// Search
	@GetMapping("/comment/{commentId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@PathVariable int commentId) throws Exception{
			
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Comment result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findById(commentId);
			commonResponse = manageResult(result, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/comment/findByDormId/{dormId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findByDormId(@PathVariable int dormId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<Comment> result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findByDormId(dormId);
			commonResponse = manageResult(result, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
}
