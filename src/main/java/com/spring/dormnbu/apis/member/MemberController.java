package com.spring.dormnbu.apis.member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;
import com.spring.dormnbu.cores.member.interfaces.MemberManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class MemberController extends CommonController {
	
	@Autowired
	private MemberManager manager;

	@Override
	public void postConstruct() {
		
	}
	
	// Add
	@PostMapping("/member")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@RequestBody Member value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Member result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}
	
	// Edit
	@PutMapping("/member")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse edit(@RequestBody Member value) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Member result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.edit(value);
			commonResponse = manageResult(result, ActionType.EDIT);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.EDIT, e);
		}
		return commonResponse;
	}
	
	// Delete
	@DeleteMapping("/member/{memberId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse delete(@PathVariable int memberId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			manager.delete(memberId);
			commonResponse = manageResult(null, ActionType.DELETE);
					
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.DELETE, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/member/{memberId}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findById(@PathVariable int memberId) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Member result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findById(memberId);
			commonResponse = manageResult(result, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
	// Search
	@GetMapping("/member")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse findByCriteria(@RequestBody CriteriaMember criteria) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		List<Member> result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.findByCriteria(criteria);
			commonResponse = manageResult(result, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}

	
}
