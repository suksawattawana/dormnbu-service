package com.spring.dormnbu.apis.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.login.interfaces.LoginManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class LoginController extends CommonController {

	@Autowired
	private LoginManager manager;
	
	@Override
	public void postConstruct() {
		
	}

	@GetMapping("/login/{user}/{pass}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse validateLogin(@PathVariable String user, @PathVariable String pass) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Member result = new Member();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.validateLogin(user, pass);
			commonResponse = manageResult(result, ActionType.SEARCH);
			
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
	
	@GetMapping("/login/{user}")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse forgetPassword(@PathVariable String user) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			
			commonResponse = manageResult(null, ActionType.SEARCH);
					
		}catch (Exception e) {
			commonResponse = manageException(null, ActionType.SEARCH, e);
		}
		return commonResponse;
	}
}
