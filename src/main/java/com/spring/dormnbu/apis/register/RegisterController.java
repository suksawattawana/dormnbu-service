package com.spring.dormnbu.apis.register;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.dormnbu.commons.CommonController;
import com.spring.dormnbu.commons.CommonResponse;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.register.domain.Register;
import com.spring.dormnbu.cores.register.interfaces.RegisterManager;
import com.spring.dormnbu.enums.ActionType;

@RestController
@RequestMapping("/api")
@Scope("request")
public class RegisterController extends CommonController {

	@Autowired
	private RegisterManager manager;
	
	@Override
	public void postConstruct() {
		
	}
	
	// Add
	@PostMapping("/register")
	@Transactional(propagation = Propagation.REQUIRED)
	public CommonResponse add(@Validated @RequestBody Register value, Errors errors) throws Exception{
		
		//1. create object
		CommonResponse commonResponse = new CommonResponse();
		Member result = null;
		
		try{
			//2.ค้นหา และจัดการผลลัพธ์
			result = manager.add(value);
			commonResponse = manageResult(result, ActionType.ADD);
					
		}catch (Exception e) {
			commonResponse = manageException(result, ActionType.ADD, e);
		}
		return commonResponse;
	}

}
