package com.spring.dormnbu.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CommonSearchResult<T extends CommonSearchCriteria> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4370958408835180375L;
	private long totalResult;
	private List<T> listResult = new ArrayList<T>();
	
	public long getTotalResult() {
		return totalResult;
	}
	public void setTotalResult(long totalResult) {
		this.totalResult = totalResult;
	}
	public List<T> getListResult() {
		return listResult;
	}
	public void setListResult(List<T> listResult) {
		this.listResult = listResult;
	}
	
	
}
