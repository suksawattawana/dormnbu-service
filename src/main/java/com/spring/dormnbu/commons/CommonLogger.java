package com.spring.dormnbu.commons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CommonLogger {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	public Exception logging(Exception e) throws Exception {
		
		// show log in console
		logger.error("" , e);
		
//		if (e instanceof IllegalArgumentException) {
//			throw new IllegalArgumentException(e.getMessage());
//		}
		
		throw e;
	}
	
}
