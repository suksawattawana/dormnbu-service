package com.spring.dormnbu.commons;

public class CommonError {

	private String errorCode; 		// error code (exception code)
	private String errorDesc; 		// error description (exception description)
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDesc() {
		return errorDesc;
	}
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}	
	
}
