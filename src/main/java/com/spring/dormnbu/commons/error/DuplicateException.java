package com.spring.dormnbu.commons.error;

public class DuplicateException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DuplicateException() {
		super();
	}

	public DuplicateException(String message) {
		super(message);
	}

}
