package com.spring.dormnbu.commons.error;

public class InputValidateException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	public InputValidateException() {
		super();
	}
	
	public InputValidateException(String message) {
		super(message);
	}

}
