package com.spring.dormnbu.commons;

import java.io.Serializable;

import com.spring.dormnbu.commons.sql.HeaderSorts;

public class CommonSearchCriteria implements Serializable{

	private static final long serialVersionUID = 1300095307394038192L;
	private HeaderSorts[] headerSorts;
	
	public HeaderSorts[] getHeaderSorts() {
		return headerSorts;
	}
	public void setHeaderSorts(HeaderSorts[] headerSorts) {
		this.headerSorts = headerSorts;
	}
	
	

}
