package com.spring.dormnbu.commons;


import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.enums.ActionType;
import com.spring.dormnbu.enums.DisplayStatus;

@ControllerAdvice
public abstract class CommonController {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	
	@PostConstruct
	public abstract void postConstruct();
	
	protected CommonResponse manageResult(Object obj, ActionType actionType) {
		CommonResponse response = new CommonResponse();
		
		// เช็คว่ามาจากฟังก์ชันไหน
		if (actionType == ActionType.SEARCH) {
			
			// ตรวจสอบค่าว่างข้อมูล
			if (obj != null) {
				response.setData(obj);
				response.setMessageCode(actionType.getMessageSuccessCode());
				response.setMessageDesc(actionType.getMessageSuccessDesc());
				response.setStatus(HttpStatus.OK.value());
				response.setDisplayStatus(DisplayStatus.SUCCESS.getValue());
			}
			else{
				response.setData(obj);
				response.setMessageCode(actionType.getMessageSuccessCode());
				response.setMessageDesc("Data cannot be found.");
				response.setStatus(HttpStatus.OK.value());
				response.setDisplayStatus(DisplayStatus.WARN.getValue());
			}
		}
		else {
			response.setData(obj);
			response.setMessageCode(actionType.getMessageSuccessCode());
			response.setMessageDesc(actionType.getMessageSuccessDesc());
			response.setStatus(HttpStatus.OK.value());
			response.setDisplayStatus(DisplayStatus.SUCCESS.getValue());
		}
		
		return response;
	}
	
	@ExceptionHandler
	protected CommonResponse manageException(Object obj, ActionType actionType, Exception exception) throws Exception {
		logger.error("Exception : " + exception.getMessage(), exception);
		
		CommonResponse response = new CommonResponse();
		try {
			if (exception != null) {
				
				CommonError error = new CommonError();
				error.setErrorDesc(exception.getMessage());
				
				response.setMessageCode(actionType.getMessageErrorCode());
				response.setMessageDesc(actionType.getMessageErrorDesc());
				response.setStatus(getStatus(exception));
				response.setDisplayStatus(DisplayStatus.ERROR.getValue());
				response.setError(error);
			}

		} catch (Exception e) {
			logger.error("Manage Exception Error");
		}
		return response;
	}
	
	/*@ExceptionHandler
	public CommonResponse handleException(Exception exception) {
		
		// create CommonResponse
		CommonResponse response = new CommonResponse();
		try {
			if (exception != null) {
				
				CommonError error = new CommonError();
				error.setErrorDesc(exception.getMessage());
				
				response.setMessageCode(null);
				response.setMessageDesc(null);
				response.setStatus(HttpStatus.BAD_REQUEST.value());
				response.setDisplayStatus(DisplayStatus.ERROR.getValue());
				response.setError(error);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return response;
	}*/
	
	protected int getStatus(Exception exception) throws Exception{
		int status = 0;
		
		try {
			if(exception instanceof DataNotFoundException){
				status = HttpStatus.NOT_FOUND.value();
			}
			else if(exception instanceof DuplicateException){
				status = HttpStatus.EXPECTATION_FAILED.value();
			}
		}catch (Exception e) {
			throw e;
		}
		
		return status;
	}
	
}
