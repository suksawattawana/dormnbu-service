package com.spring.dormnbu.configs;

import java.nio.charset.StandardCharsets;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.spring.dormnbu.enums.ImageContentType;


@Configuration
@EnableWebMvc
@ComponentScans(value = { 
	@ComponentScan(basePackages = "com.spring.dormnbu.apis.*")
})
public class RestfulConfiguration implements WebMvcConfigurer {
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		
		try {
			// JSON
			Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
			builder.indentOutput(true);
			builder.serializationInclusion(Include.NON_NULL);
				        
			MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter(builder.build());
			converter.setDefaultCharset(StandardCharsets.UTF_8);
			converters.add(converter);
			
			// Image
	        ByteArrayHttpMessageConverter byteArrayConverter = new ByteArrayHttpMessageConverter();
	        byteArrayConverter.setSupportedMediaTypes(ImageContentType.getAllMediaType());
			converters.add(byteArrayConverter);
			
			WebMvcConfigurer.super.configureMessageConverters(converters);
			
		} catch (Exception e) {
			
		}
	}
}
