package com.spring.dormnbu.configs;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:secure.properties")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private Environment env;
	
	private static String SPLIT_REGEX = ",";
	private static boolean requiresSecure;

	public static boolean isRequiresSecure() {
		return requiresSecure;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// Set HTTPS Only
		String secure = env.getProperty("specific.requiresSecure");
		if(secure != null && Boolean.parseBoolean(secure)) {
			http.requiresChannel().anyRequest().requiresSecure();
			requiresSecure = true;
		}
				
		http
		
		// Configuring exception handling
		.exceptionHandling()
		.authenticationEntryPoint(new Http403ForbiddenEntryPoint())
		
		// Cross Origin Resource Sharing
		.and()
		.cors()
		
		// Cross Site Request Forgery And Check Authorize
		.and()
		.csrf().disable()
		.authorizeRequests()
		.antMatchers(HttpMethod.GET, "/api/**").permitAll()
		.antMatchers(HttpMethod.POST, "/api/**").permitAll()					
		.antMatchers(HttpMethod.PUT, "/api/**").permitAll()
		.antMatchers(HttpMethod.PATCH, "/api/**").permitAll()
		.antMatchers(HttpMethod.DELETE, "/api/**").permitAll()
		.anyRequest().denyAll()
		.anyRequest().permitAll()
		
		// Never create and never use HttpSession
		.and()
		.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
		
	}
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		
		try {
			final CorsConfiguration configuration = new CorsConfiguration();
			configuration.setAllowedOrigins(Arrays.asList(env.getProperty("cors.origins").split(SPLIT_REGEX)));
			configuration.setAllowedMethods(Arrays.asList(env.getProperty("cors.methods").split(SPLIT_REGEX)));
			configuration.setAllowedHeaders(Arrays.asList(env.getProperty("cors.headers").split(SPLIT_REGEX)));
			configuration.setAllowCredentials(true);
			
			source.registerCorsConfiguration("/api/**", configuration);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return source;
	}
	
}
