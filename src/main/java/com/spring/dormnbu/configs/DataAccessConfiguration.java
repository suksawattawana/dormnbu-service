package com.spring.dormnbu.configs;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.spring.dormnbu.commons.sql.SQLUtils;


@Configuration
@EnableTransactionManagement
@PropertySource("classpath:db.properties")
@ComponentScans(value = {
	@ComponentScan("com.spring.dormnbu.cores.login.services")
	, @ComponentScan("com.spring.dormnbu.cores.register.services")
	, @ComponentScan("com.spring.dormnbu.cores.member.services")
	, @ComponentScan("com.spring.dormnbu.cores.dorm.services")
	, @ComponentScan("com.spring.dormnbu.cores.favorite.services")
	, @ComponentScan("com.spring.dormnbu.cores.promotion.services")
	, @ComponentScan("com.spring.dormnbu.cores.review.services")
	, @ComponentScan("com.spring.dormnbu.cores.comment.services")
	, @ComponentScan("com.spring.dormnbu.cores.compare.services")
	, @ComponentScan("com.spring.dormnbu.cores.report.services")
	
})
public class DataAccessConfiguration extends SQLUtils {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private Environment env;
	
	/*
	 * ตัวหลัก
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean() throws PropertyVetoException {
		LocalContainerEntityManagerFactoryBean factoryBean = null;
		
		try {
			// create Object
			factoryBean = new LocalContainerEntityManagerFactoryBean();
			
			// set the properties
			factoryBean.setDataSource(dataSource());
			factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
			factoryBean.setJpaProperties(jpaProperties());
			factoryBean.setPersistenceUnitName("dormnbuPersistenceUnit");
			factoryBean.setPackagesToScan("com.spring.dormnbu.cores.entity");
			
			String[] mappingResources = {
				"sql/login/QueryLogin.xml"
				, "sql/register/QueryRegister.xml"
				, "sql/member/QueryMember.xml"
				, "sql/favorite/QueryFavorite.xml"
				, "sql/dorm/QueryDorm.xml"
				, "sql/promotion/QueryPromotion.xml"
				, "sql/review/QueryReview.xml"
				, "sql/comment/QueryCommment.xml"
				, "sql/compare/QueryCompare.xml"
				, "sql/report/QueryReport.xml"
			};
			
			factoryBean.setMappingResources(mappingResources);
			
			// Keep SQL
			SQLUtils.init(mappingResources);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return factoryBean;
	}
	
	/*
	 * เชื่อมต่อฐานข้อมูล โดยอ้างจาก properties
	 */
	@Bean
	public DataSource dataSource() throws PropertyVetoException {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		
		logger.info("jdbcUrl: " + env.getProperty("database.jdbcUrl"));
		logger.info("username: "+ env.getProperty("database.username"));
		
		// Setting JDBC properties
		dataSource.setDriverClass(env.getProperty("database.driver"));
		dataSource.setJdbcUrl(env.getProperty("database.jdbcUrl"));
		dataSource.setUser(env.getProperty("database.username"));
		dataSource.setPassword(env.getProperty("database.password"));
		
		// set connection pool props
		dataSource.setInitialPoolSize(getIntProperty("connection.pool.initialPoolSize"));
		dataSource.setMinPoolSize(getIntProperty("connection.pool.minPoolSize"));
		dataSource.setMaxPoolSize(getIntProperty("connection.pool.maxPoolSize"));		
		dataSource.setMaxIdleTime(getIntProperty("connection.pool.maxIdleTime"));
		
		return dataSource;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() throws PropertyVetoException {
		JpaTransactionManager transactionManager = new JpaTransactionManager(entityManagerFactoryBean().getObject());
		return transactionManager;
	}
	
	@Bean
    public JpaVendorAdapter jpaVendorAdapter() {
		JpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		return adapter;
    }
	
	private Properties jpaProperties() {
		Properties props = new Properties();
		props.put(org.hibernate.cfg.Environment.JDBC_TIME_ZONE, "UTC");
 		props.put(org.hibernate.cfg.Environment.DEFAULT_SCHEMA, env.getProperty("database.schema"));
		props.put(org.hibernate.cfg.Environment.DIALECT, env.getProperty("hibernate.dialect"));
		props.put(org.hibernate.cfg.Environment.SHOW_SQL, env.getProperty("hibernate.show_sql"));
		props.put(org.hibernate.cfg.Environment.FORMAT_SQL, env.getProperty("hibernate.format_sql"));
		props.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, env.getProperty("hibernate.hbm2ddl.auto"));		
		return props;
	}
	
	private int getIntProperty(String propName) {
		return Integer.parseInt(env.getProperty(propName));
	}	
	
}
