package com.spring.dormnbu.configs;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;


@Configuration
@ComponentScans(value = {
	@ComponentScan("com.spring.dormnbu.utils")
	, @ComponentScan("com.spring.dormnbu.commons")
})
public class ApplicationConfiguration {
	
	@Bean
	public MessageSource messageSource() {
		ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("bundle/common/MessageAlert");
        messageSource.setDefaultEncoding("UTF-8");
        
        return messageSource;
	}
}
