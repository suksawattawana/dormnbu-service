package com.spring.dormnbu.enums;

public enum MemberType {
	
	Admin("A","ผู้ดูแลระบบ"),
	Member("M","ผู้ใช้งาน"),
	DORM_ADMIN("D","ผู้ดูแลหอพัก");

	private String key;
	private String value;
	
	private MemberType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public static String findValue(String key){
		String result = null;
		
		for (MemberType data : MemberType.values()) {
			if (data.getKey().equals(key)) {
				result = data.getValue();
			}
		}
		return result;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
	
}
