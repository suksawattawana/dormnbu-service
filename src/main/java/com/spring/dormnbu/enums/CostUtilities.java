package com.spring.dormnbu.enums;

public enum CostUtilities {
	
	DEPOSIT_HAVE("D","ระบุเงินประกัน"),
	DEPOSIT_NOT("I","ไม่มีเงินประกัน"),
	
	INTERNET_HAVE("H","มีอินเตอร์เน็ต"),
	INTERNET_NOT("N","ไม่มีอินเตอร์เน็ต"),
	INTERNET_CONTACT("C","โทรสอบถาม"),
	INTERNET_FREE("F","ฟรี"),
	
	UNIT_USE("U","ตามยูนิตที่ใช้"),
	PACKAGES("P","เหมาจ่าย"),
	CONTACT("C","โทรสอบถาม");
	
	
	
	private String key;
	private String value;
	
	private CostUtilities(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	public static String findValue(String key){
		String result = null;
		
		for (CostUtilities data : CostUtilities.values()) {
			if (data.getKey().equals(key)) {
				result = data.getValue();
			}
		}
		return result;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
