package com.spring.dormnbu.enums;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;

public enum ImageContentType {

	PDF("PDF", MediaType.APPLICATION_PDF , MediaType.APPLICATION_PDF_VALUE)
	, OCTET_STREAM("OCTET_STREAM", MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_OCTET_STREAM_VALUE)
	, GIF("GIF", MediaType.IMAGE_GIF, MediaType.IMAGE_GIF_VALUE)
	, JPG("JPG", MediaType.IMAGE_JPEG, MediaType.IMAGE_JPEG_VALUE)
	, JPEG("JPEG", MediaType.IMAGE_JPEG, MediaType.IMAGE_JPEG_VALUE)
	, PNG("PNG", MediaType.IMAGE_PNG, MediaType.IMAGE_PNG_VALUE)
	, BMP("BMP", MediaType.valueOf("image/bmp"), "image/bmp");

	private String extension;
	private MediaType mediaType;
	private String contentType;
	
	private ImageContentType(String extension, MediaType mediaType, String contentType) {
		this.extension = extension;
		this.mediaType = mediaType;
		this.contentType = contentType;
	}
	
	public String getExtension() {
		return extension;
	}
	
	public MediaType getMediaType() {
		return mediaType;
	}
	
	public String getContentType() {
		return contentType;
	}
	
	public static List<MediaType> getAllMediaType() {
		List<MediaType> result = new ArrayList<MediaType>();
		for (ImageContentType type : ImageContentType.values()) {
			if (result.contains(type.mediaType)) {
				continue;
			}
			result.add(type.mediaType);
		}
	
		return result;
	}
	
	public static ImageContentType getTypeByExtension(String extension) {
		ImageContentType result = null;
	
		if (extension != null && !extension.isEmpty()) {
			String taget = extension.toUpperCase();
			for (ImageContentType type : ImageContentType.values()) {
				if (type.extension.equals(taget)) {
					result = type;
					break;
				}
			}
		}
	
		return result;
	}
	
}
