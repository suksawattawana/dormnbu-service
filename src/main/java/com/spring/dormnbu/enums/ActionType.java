package com.spring.dormnbu.enums;

public enum ActionType {

	DEFAULT("X30001", "Process completed.", "X30006", "Process not completed.")
	, SEARCH("X30001", "Process completed.", "X30006", "Process not completed.")
	, ADD("X30002", "Save data completed.", "X30007", "Save data not completed.")
	, EDIT("X30003", "Save change data completed.", "X30008", "Save change data not completed.")
	, VIEW("X30004", "Process completed.", "X30009", "Process not completed.")
	, DELETE("X30005", "Delete data completed.", "X30010", "Delete data not completed.")
	;

	private String messageSuccessCode;
	private String messageSuccessDesc;
	private String messageErrorCode;
	private String messageErrorDesc;

	private ActionType(String messageSuccessCode, String messageSuccessDesc
			, String messageErrorCode, String messageErrorDesc) {
		
		this.messageSuccessCode = messageSuccessCode;
		this.messageSuccessDesc = messageSuccessDesc;
		this.messageErrorCode = messageErrorCode;
		this.messageErrorDesc = messageErrorDesc;
	
	}
	
	public String getMessageErrorCode() {
		return messageErrorCode;
	}

	public String getMessageErrorDesc() {
		return messageErrorDesc;
	}
	
	public String getMessageSuccessCode() {
		return messageSuccessCode;
	}

	public String getMessageSuccessDesc() {
		return messageSuccessDesc;
	}
}
