package com.spring.dormnbu.cores.domain;

import java.io.Serializable;

public class Member implements Serializable {
	
	private static final long serialVersionUID = -3951581842100068274L;
	private int memberId;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String password;
	private String picture;
	private String certficate;
	private String createDate;
	private String modify;
	private Character status;
	private Character memberType;
	private String memberTypeDesc;
	
	
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getCertficate() {
		return certficate;
	}
	public void setCertficate(String certficate) {
		this.certficate = certficate;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getModify() {
		return modify;
	}
	public void setModify(String modify) {
		this.modify = modify;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	public Character getMemberType() {
		return memberType;
	}
	public void setMemberType(Character memberType) {
		this.memberType = memberType;
	}
	public String getMemberTypeDesc() {
		return memberTypeDesc;
	}
	public void setMemberTypeDesc(String memberTypeDesc) {
		this.memberTypeDesc = memberTypeDesc;
	}
	
	
}
