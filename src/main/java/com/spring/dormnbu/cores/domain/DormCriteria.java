package com.spring.dormnbu.cores.domain;

import com.spring.dormnbu.commons.CommonSearchCriteria;

public class DormCriteria extends CommonSearchCriteria {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dormCode;
	private String nameTh;
	private String createDate;

	public String getDormCode() {
		return dormCode;
	}

	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}

	public String getNameTh() {
		return nameTh;
	}

	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	
}
