package com.spring.dormnbu.cores.review.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.entity.ReviewEntity;
import com.spring.dormnbu.cores.review.domain.Review;
import com.spring.dormnbu.cores.review.interfaces.ReviewDao;

@Repository
@Transactional
@Scope("request")
public class ReviewDaoImp implements ReviewDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public ReviewEntity checkDup(Review value) throws Exception {
		ReviewEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Review.checkDup", ReviewEntity.class);
			query.setParameter("dormId", value.getDormId()); 
			query.setParameter("memberId", value.getMemberId()); 
			
			//2. query
			List<ReviewEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
		
	@Override
	public ReviewEntity add(ReviewEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;	
	}
	
	@Override
	public ReviewEntity edit(ReviewEntity value) throws Exception {
		try{
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;    
	}
	
	@Override
	public void delete(ReviewEntity value) throws Exception {	
		try{
			em.remove(value);
		}catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public ReviewEntity findById(int id) throws Exception {
		ReviewEntity result = null;
			
		try{
			result = em.find(ReviewEntity.class, id);	
		}catch (Exception e) {
			throw e;
		}
			
		return result;
	}

}
