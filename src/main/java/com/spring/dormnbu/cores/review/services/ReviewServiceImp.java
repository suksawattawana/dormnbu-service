package com.spring.dormnbu.cores.review.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.entity.ReviewEntity;
import com.spring.dormnbu.cores.member.interfaces.MemberDao;
import com.spring.dormnbu.cores.review.domain.Review;
import com.spring.dormnbu.cores.review.interfaces.ReviewDao;
import com.spring.dormnbu.cores.review.interfaces.ReviewService;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class ReviewServiceImp implements ReviewService {
	
	@Autowired
	private ReviewDao dao;
	
	@Autowired
	private DormDao dormDao;
	
	@Autowired
	private MemberDao memberDao;
	
	@Override
	public boolean checkDup(Review value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			ReviewEntity data = dao.checkDup(value);
			
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public boolean checkNullData(Review value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			Integer memberId =  memberDao.checkNullMember(value.getMemberId());
			Integer dormId = dormDao.checkNullDorm(value.getDormId());
			
			if (memberId != 0 && dormId != 0){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Review add(Review value) throws Exception {
		Review result = null;
		
		try {
			//1. จัดกาข้อมูล
			ReviewEntity data = new ReviewEntity();
			BeanUtils.copyProperties(data, value);
			data.setCreateDate(DateTimeUtil.getDateCurentDate());
				
			//2. อัพเดทข้อมูล
			data = dao.add(data);
				
			//3. return ข้อมุล
			result = new Review();
			BeanUtils.copyProperties(result, data);
			result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));

		} catch (Exception e) {
			throw e;
		}
			
		return result;
	}
	
	@Override
	public Review edit(Review value) throws Exception {
		Review result = null;
		
		try {
			//1. ค้นหาข้อมูล
			ReviewEntity data = dao.findById(value.getReviewId());
			
			//2. จัดการผลลัพธ์
			if (data != null){
				
				//2.1 ข้อมูลที่มีการแก้ไข
				data.setScore(value.getScore());
				data.setModify(DateTimeUtil.getDateCurentDate());
				
				//3. อัพเดทข้อมูล
				dao.edit(data);
				
				//4. return ข้อมุล
				result = new Review();
				BeanUtils.copyProperties(result, data);
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Review findById(int id) throws Exception {
		Review result = null;
		
		try {
			//1. ค้นหาข้อมูล
			ReviewEntity data = dao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Review();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
				
				//2.2 ข้อมูลเพิ่มเติม
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ค้นหาข้อมูล
			ReviewEntity entity = dao.findById(id);
			
			//2. ลบข้อมูล
			if (entity != null){
				dao.delete(entity);
			}
			else{
				throw new DataNotFoundException("No Data for key: " + id);
			}
			
		} catch (Exception e) {
			throw e;
		}
	}

}
