package com.spring.dormnbu.cores.review.interfaces;

import com.spring.dormnbu.cores.entity.ReviewEntity;
import com.spring.dormnbu.cores.review.domain.Review;

public interface ReviewDao {
	
	ReviewEntity checkDup(Review value) throws Exception;
	
	ReviewEntity add(ReviewEntity value) throws Exception;
	
	ReviewEntity edit(ReviewEntity value) throws Exception;
	
	ReviewEntity findById(int id) throws Exception;
	
	void delete(ReviewEntity value) throws Exception;
	
}
