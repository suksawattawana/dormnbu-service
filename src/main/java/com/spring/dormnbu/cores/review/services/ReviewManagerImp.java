package com.spring.dormnbu.cores.review.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.cores.review.domain.Review;
import com.spring.dormnbu.cores.review.interfaces.ReviewManager;
import com.spring.dormnbu.cores.review.interfaces.ReviewService;

@Component
@Transactional
@Scope("request")
public class ReviewManagerImp implements ReviewManager{
	
	@Autowired
	private ReviewService service;
	
	@Override
	public Review add(Review value) throws Exception {
		Review result = null;

		try {
			//1. ตรวจสอบ criteria
			if (value != null) {
				
				//2. ตรวจสอบค่าว่าง
				boolean checkNull = service.checkNullData(value);
				if (checkNull){
					
					//3. ตรวจสอบข้อมูลซ้ำ
					boolean checkDup = service.checkDup(value);
					if (!checkDup){
						result = service.add(value);
					}else{
						throw new DuplicateException("Duplicate Data!");
					}
				}
				else{
					throw new DataNotFoundException("Data Not Found!");
				}
				
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Review edit(Review value) throws Exception {
		Review result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getReviewId() != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.edit(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Review findById(int id) throws Exception {
		Review result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findById(id);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ลบข้อมูล
				service.delete(id);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
}
