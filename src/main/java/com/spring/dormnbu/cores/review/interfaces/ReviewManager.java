package com.spring.dormnbu.cores.review.interfaces;

import com.spring.dormnbu.cores.review.domain.Review;

public interface ReviewManager {
	Review add(Review value) throws Exception;
	Review  edit(Review  value) throws Exception;
	Review findById(int id) throws Exception;
	void delete(int id) throws Exception;
}
