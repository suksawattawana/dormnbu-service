package com.spring.dormnbu.cores.compare.interfaces;

import com.spring.dormnbu.cores.compare.domian.DormResult;

public interface CompareDao {

	DormResult findDormById(int dormId) throws Exception;
}
