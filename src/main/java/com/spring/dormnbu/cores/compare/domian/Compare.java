package com.spring.dormnbu.cores.compare.domian;

import java.io.Serializable;

public class Compare implements Serializable {

	private static final long serialVersionUID = 6574917873596685431L;
	private int[] dormIds;
	
	public int[] getDormIds() {
		return dormIds;
	}
	public void setDormIds(int[] dormIds) {
		this.dormIds = dormIds;
	}
	
	
}
