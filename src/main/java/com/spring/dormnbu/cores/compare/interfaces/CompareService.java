package com.spring.dormnbu.cores.compare.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.compare.domian.DormResult;

public interface CompareService {

	List<DormResult> compareDorm(int[] ids) throws Exception;
}
