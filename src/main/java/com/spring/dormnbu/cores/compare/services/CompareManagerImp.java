package com.spring.dormnbu.cores.compare.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.compare.domian.Compare;
import com.spring.dormnbu.cores.compare.domian.DormResult;
import com.spring.dormnbu.cores.compare.interfaces.CompareManager;
import com.spring.dormnbu.cores.compare.interfaces.CompareService;

@Component
@Transactional
@Scope("request")
public class CompareManagerImp implements CompareManager {

	@Autowired
	private CompareService service;

	@Override
	public List<DormResult> compareDorm(Compare compare) throws Exception {
		List<DormResult> result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (compare != null  && compare.getDormIds().length <= 3) {
				
				//2. ค้นหาข้อมูล
				result = service.compareDorm(compare.getDormIds());
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
}
