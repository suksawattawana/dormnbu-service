package com.spring.dormnbu.cores.compare.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.compare.domian.Compare;
import com.spring.dormnbu.cores.compare.domian.DormResult;

public interface CompareManager {

	List<DormResult> compareDorm(Compare compare) throws Exception;
}
