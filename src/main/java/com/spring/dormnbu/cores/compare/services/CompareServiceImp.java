package com.spring.dormnbu.cores.compare.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.compare.domian.DormResult;
import com.spring.dormnbu.cores.compare.interfaces.CompareDao;
import com.spring.dormnbu.cores.compare.interfaces.CompareService;

@Service
@Transactional
@Scope("request")
public class CompareServiceImp implements CompareService{

	@Autowired
	private CompareDao dao;
	
	@Override
	public List<DormResult> compareDorm(int[] ids) throws Exception {
		List<DormResult> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<DormResult> datas = new ArrayList<DormResult>();
			for (int id : ids) {
				datas.add(dao.findDormById(id));
			}
			
			//2. จัดการผลลัพธ์
			if (datas != null && !datas.isEmpty()) {
				
				// ค่าเริ่มสำหรับเปรียบเทียบ
				BigDecimal zeroVal = new BigDecimal(0);
				int winWater = datas.get(0).getWaterPrice();
				int winElectricity = datas.get(0).getElectricityPrice();
				int winOtherService = datas.get(0).getOtherServicePrice();
				int winInternete = datas.get(0).getInternetPrice();
				int winDeposit = datas.get(0).getDepositPrice();
				BigDecimal winMonthlyRent = datas.get(0).getMonthlyMin();
				BigDecimal winDailyRent = datas.get(0).getDailyMin();
				
				// หาค่าน้อยที่สุด
				for (DormResult data : datas) {
					
					if (data.getWaterPrice() <= winWater){
						winWater = data.getWaterPrice();
					}
					if (data.getElectricityPrice() <= winElectricity){
						winElectricity = data.getElectricityPrice();
					}
					if (data.getOtherServicePrice() <= winOtherService){
						winOtherService = data.getOtherServicePrice();
					}
					if (data.getInternetPrice() <= winInternete){
						winInternete = data.getInternetPrice();
					}
					if (data.getDepositPrice() <= winDeposit){
						winDeposit = data.getDepositPrice();
					}
					if (data.getMonthlyMin() != null && data.getMonthlyMin() != zeroVal){
						if (data.getMonthlyMin().intValue() < winMonthlyRent.intValue()){
							winMonthlyRent = data.getMonthlyMin();
						}
					}
					if (data.getDailyMin() != null && data.getDailyMin() != zeroVal){
						if (data.getDailyMin().intValue() < winDailyRent.intValue()){
							winDailyRent = data.getDailyMin();
						}
					}
				}
				
				// create object
				result = new ArrayList<>();
				for (DormResult data : datas) {
					
					if (data.getWaterPrice() == winWater){
						data.setWinWater('Y');
					}
					if (data.getElectricityPrice() == winElectricity){
						data.setWinElectricity('Y');
					}
					if (data.getOtherServicePrice() == winOtherService){
						data.setWinOtherService('Y');
					}
					if (data.getInternetPrice() == winInternete){
						data.setWinInternete('Y');
					}
					if (data.getDepositPrice() == winDeposit){
						data.setWinDeposit('Y');
					}
					if(data.getMonthlyMin() != null){
						if (data.getMonthlyMin().equals(winMonthlyRent)){
							data.setWinMonthlyRent('Y');
						}
					}
					if(data.getDailyMin() != null){
						if (data.getDailyMin().equals(winDailyRent) && data.getDailyMin() != null){
							data.setWinDailyRent('Y');
						}
					}
					
					result.add(data);
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
}
