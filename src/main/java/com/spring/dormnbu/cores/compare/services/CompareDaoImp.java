package com.spring.dormnbu.cores.compare.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.sql.IgnoreCaseAliasToBeanResultTransformer;
import com.spring.dormnbu.cores.compare.domian.DormResult;
import com.spring.dormnbu.cores.compare.interfaces.CompareDao;

@Repository
@Transactional
@Scope("request")
public class CompareDaoImp implements CompareDao{

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public DormResult findDormById(int dormId) throws Exception {
		DormResult result = null;
		
		try{
			//1. map sql
			Query query = em.createNamedQuery("Compare.findDormById");
			query.setParameter("dormId", dormId); 
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(DormResult.class));
			
			//2. query
			List<DormResult> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
}
