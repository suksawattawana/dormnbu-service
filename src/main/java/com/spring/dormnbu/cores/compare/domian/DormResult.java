package com.spring.dormnbu.cores.compare.domian;

import java.io.Serializable;
import java.math.BigDecimal;

public class DormResult implements Serializable {

	private static final long serialVersionUID = -5960727127180564106L;
	private int dormId;
	private String name;
	private String picture;
	private String detail;
	private BigDecimal score;
	private String promotionDetail;
	private int totalCost; //ค่าใช้ทั้งทั้งหมด
	
	private Character waterType; //ค่าน้ำ
	private int waterMax;
	private int waterMin;
	private int waterPrice;
	private Character electricityType; //ค่าไฟ
	private int electricityMax;
	private int electricityMin;
	private int electricityPrice;
	private Character otherServiceType; //ค่าบริการอื่นๆ
	private String otherServiceDesc;
	private int otherServicePrice;
	private Character internetType; //ค่าอินเทอร์เน็ต
	private int internetPrice;
	private Character depositType; //เงินมัดจำ
	private int depositPrice;
	private Character payadvanceType; //จ่ายล่วงหน้า
	private int payadvancePrice;
	private Character monthlyRent; //ค่าห้องรายเดือน
	private BigDecimal monthlyMax;
	private BigDecimal monthlyMin;
	private Character dailyRent; //ค่าห้องรายวัน
	private BigDecimal dailyMax;
	private BigDecimal dailyMin;
	
	private Character winWater = 'N';
	private Character winElectricity = 'N';
	private Character winOtherService = 'N';
	private Character winInternete = 'N';
	private Character winDeposit = 'N';
	private Character winMonthlyRent = 'N';
	private Character winDailyRent = 'N';
	
	public int getDormId() {
		return dormId;
	}
	public void setDormId(int dormId) {
		this.dormId = dormId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public String getPromotionDetail() {
		return promotionDetail;
	}
	public void setPromotionDetail(String promotionDetail) {
		this.promotionDetail = promotionDetail;
	}
	public int getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}
	public Character getWaterType() {
		return waterType;
	}
	public void setWaterType(Character waterType) {
		this.waterType = waterType;
	}
	public int getWaterMax() {
		return waterMax;
	}
	public void setWaterMax(int waterMax) {
		this.waterMax = waterMax;
	}
	public int getWaterMin() {
		return waterMin;
	}
	public void setWaterMin(int waterMin) {
		this.waterMin = waterMin;
	}
	public int getWaterPrice() {
		return waterPrice;
	}
	public void setWaterPrice(int waterPrice) {
		this.waterPrice = waterPrice;
	}
	public Character getElectricityType() {
		return electricityType;
	}
	public void setElectricityType(Character electricityType) {
		this.electricityType = electricityType;
	}
	public int getElectricityMax() {
		return electricityMax;
	}
	public void setElectricityMax(int electricityMax) {
		this.electricityMax = electricityMax;
	}
	public int getElectricityMin() {
		return electricityMin;
	}
	public void setElectricityMin(int electricityMin) {
		this.electricityMin = electricityMin;
	}
	public int getElectricityPrice() {
		return electricityPrice;
	}
	public void setElectricityPrice(int electricityPrice) {
		this.electricityPrice = electricityPrice;
	}
	public Character getOtherServiceType() {
		return otherServiceType;
	}
	public void setOtherServiceType(Character otherServiceType) {
		this.otherServiceType = otherServiceType;
	}
	public String getOtherServiceDesc() {
		return otherServiceDesc;
	}
	public void setOtherServiceDesc(String otherServiceDesc) {
		this.otherServiceDesc = otherServiceDesc;
	}
	public int getOtherServicePrice() {
		return otherServicePrice;
	}
	public void setOtherServicePrice(int otherServicePrice) {
		this.otherServicePrice = otherServicePrice;
	}
	public Character getInternetType() {
		return internetType;
	}
	public void setInternetType(Character internetType) {
		this.internetType = internetType;
	}
	public int getInternetPrice() {
		return internetPrice;
	}
	public void setInternetPrice(int internetPrice) {
		this.internetPrice = internetPrice;
	}
	public Character getDepositType() {
		return depositType;
	}
	public void setDepositType(Character depositType) {
		this.depositType = depositType;
	}
	public int getDepositPrice() {
		return depositPrice;
	}
	public void setDepositPrice(int depositPrice) {
		this.depositPrice = depositPrice;
	}
	public Character getPayadvanceType() {
		return payadvanceType;
	}
	public void setPayadvanceType(Character payadvanceType) {
		this.payadvanceType = payadvanceType;
	}
	public int getPayadvancePrice() {
		return payadvancePrice;
	}
	public void setPayadvancePrice(int payadvancePrice) {
		this.payadvancePrice = payadvancePrice;
	}
	public Character getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(Character monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	public BigDecimal getMonthlyMax() {
		return monthlyMax;
	}
	public void setMonthlyMax(BigDecimal monthlyMax) {
		this.monthlyMax = monthlyMax;
	}
	public BigDecimal getMonthlyMin() {
		return monthlyMin;
	}
	public void setMonthlyMin(BigDecimal monthlyMin) {
		this.monthlyMin = monthlyMin;
	}
	public Character getDailyRent() {
		return dailyRent;
	}
	public void setDailyRent(Character dailyRent) {
		this.dailyRent = dailyRent;
	}
	public BigDecimal getDailyMax() {
		return dailyMax;
	}
	public void setDailyMax(BigDecimal dailyMax) {
		this.dailyMax = dailyMax;
	}
	public BigDecimal getDailyMin() {
		return dailyMin;
	}
	public void setDailyMin(BigDecimal dailyMin) {
		this.dailyMin = dailyMin;
	}
	public Character getWinWater() {
		return winWater;
	}
	public void setWinWater(Character winWater) {
		this.winWater = winWater;
	}
	public Character getWinElectricity() {
		return winElectricity;
	}
	public void setWinElectricity(Character winElectricity) {
		this.winElectricity = winElectricity;
	}
	public Character getWinOtherService() {
		return winOtherService;
	}
	public void setWinOtherService(Character winOtherService) {
		this.winOtherService = winOtherService;
	}
	public Character getWinInternete() {
		return winInternete;
	}
	public void setWinInternete(Character winInternete) {
		this.winInternete = winInternete;
	}
	public Character getWinDeposit() {
		return winDeposit;
	}
	public void setWinDeposit(Character winDeposit) {
		this.winDeposit = winDeposit;
	}
	public Character getWinMonthlyRent() {
		return winMonthlyRent;
	}
	public void setWinMonthlyRent(Character winMonthlyRent) {
		this.winMonthlyRent = winMonthlyRent;
	}
	public Character getWinDailyRent() {
		return winDailyRent;
	}
	public void setWinDailyRent(Character winDailyRent) {
		this.winDailyRent = winDailyRent;
	}
	
	
}
