package com.spring.dormnbu.cores.login.interfaces;

import com.spring.dormnbu.cores.entity.MemberEntity;

public interface LoginDao {

	MemberEntity queryMember(String user, String pass) throws Exception;
	
}
