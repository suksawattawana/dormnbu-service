package com.spring.dormnbu.cores.login.interfaces;

import com.spring.dormnbu.cores.domain.Member;

public interface LoginManager {

	Member validateLogin(String user, String pass) throws Exception;
	
	void forgetPassword(String email) throws Exception;
	
}
