package com.spring.dormnbu.cores.login.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.login.interfaces.LoginDao;

@Repository
@Transactional
@Scope("request")
public class LoginDaoImp extends CommonLogger implements LoginDao {

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public MemberEntity queryMember(String user, String pass) throws Exception {
		MemberEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Login.validateLogin", MemberEntity.class);
			query.setParameter("email", user); 
			query.setParameter("password", pass);
			
			//2. query
			List<MemberEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			logging(e);
		}
		
		return result;
	}

}
