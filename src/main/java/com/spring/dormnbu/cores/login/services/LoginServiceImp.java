package com.spring.dormnbu.cores.login.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.login.interfaces.LoginDao;
import com.spring.dormnbu.cores.login.interfaces.LoginService;
import com.spring.dormnbu.enums.MemberType;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class LoginServiceImp extends CommonLogger implements LoginService {

	@Autowired
	private LoginDao dao;
	
	@Override
	public Member queryMember(String user, String pass) throws Exception {
		Member result = null;
		
		try {
			//1. ค้นหาข้อมูล
			MemberEntity data = dao.queryMember(user, pass);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Member();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
				
				//2.2 ข้อมูลเพิ่ม
				result.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			logging(e);
		}
		
		return result;
	}

}
