package com.spring.dormnbu.cores.login.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.login.interfaces.LoginManager;
import com.spring.dormnbu.cores.login.interfaces.LoginService;
import com.spring.dormnbu.cores.member.interfaces.MemberService;

@Component
@Transactional
@Scope("request")
public class LoginMangerImp extends CommonLogger implements LoginManager {
	
	@Autowired
	private LoginService service;
	
	@Autowired
	private MemberService memberService;
	
	@Override
	public Member validateLogin(String user, String pass) throws Exception {
		Member result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (user != null && pass != null) {
				
				//2. ค้นหาข้อมูล
				result =  service.queryMember(user, pass);
			}
		} catch (Exception e) {
			logging(e);
		}
		
		return result;
	}

	@Override
	public void forgetPassword(String email) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (email != null && email != "") {
				
				//2. ค้นหาข้อมูล
				Member value = new Member();
				value.setEmail(email);
				boolean result =  memberService.checkDup(value);
				
				//3. ตรวจสอบ email
				if(result == true){
					
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

}
