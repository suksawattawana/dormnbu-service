package com.spring.dormnbu.cores.login.interfaces;

import com.spring.dormnbu.cores.domain.Member;

public interface LoginService {

	Member queryMember(String user, String pass) throws Exception;
	
}
