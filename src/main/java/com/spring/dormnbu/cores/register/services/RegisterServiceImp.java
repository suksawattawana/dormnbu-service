package com.spring.dormnbu.cores.register.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.register.domain.Register;
import com.spring.dormnbu.cores.register.interfaces.RegisterDao;
import com.spring.dormnbu.cores.register.interfaces.RegisterService;
import com.spring.dormnbu.enums.MemberType;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class RegisterServiceImp extends CommonLogger implements RegisterService  {
	
	@Autowired
	private RegisterDao dao;

	@Override
	public boolean checkDup(Register value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			MemberEntity data = dao.checkDup(value);
			
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			logging(e);
		}
		
		return result;
	}

	@Override
	public Member add(Register value) throws Exception {
		Member result = null;
		
		try {
			//1. จัดกาข้อมูล
			MemberEntity data = new MemberEntity();
			BeanUtils.copyProperties(data, value);
			data.setMemberType(MemberType.Member.getKey().charAt(0));
			data.setStatus('Y');
			data.setCreateDate(DateTimeUtil.getDateCurentDate());
			
			//2. อัพเดทข้อมูล
			data = dao.add(data);
			
			//3. return ข้อมุล
			result = new Member();
			BeanUtils.copyProperties(result, data);
			result.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
			result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
			result.setModify(DateTimeUtil.dateToString(data.getModify()));
			
		} catch (Exception e) {
			logging(e);
		}
		
		return result;
	}
}
