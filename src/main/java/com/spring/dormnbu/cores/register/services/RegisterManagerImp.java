package com.spring.dormnbu.cores.register.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.register.domain.Register;
import com.spring.dormnbu.cores.register.interfaces.RegisterManager;
import com.spring.dormnbu.cores.register.interfaces.RegisterService;

@Component
@Transactional
@Scope("request")
public class RegisterManagerImp extends CommonLogger implements RegisterManager  {
	
	@Autowired
	private RegisterService service;
	
	@Override
	public Member add(Register value) throws Exception {
		Member result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null) {
				
				//2. ตรวจสอบข้อมูลซ้ำ
				boolean checkDup = service.checkDup(value);
				
				if (checkDup == false){
					result = service.add(value);
				}
				else{
					throw new DuplicateException("This email is already in use.");
				}
			}
		} catch (Exception e) {
			logging(e);
		}
		
		return result;
	}
}
