package com.spring.dormnbu.cores.register.services;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.CommonLogger;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.register.domain.Register;
import com.spring.dormnbu.cores.register.interfaces.RegisterDao;

@Repository
@Transactional
@Scope("request")
public class RegisterDaoImp extends CommonLogger implements RegisterDao  {
	
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public MemberEntity checkDup(Register value) throws Exception {
		MemberEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Member.checkDup", MemberEntity.class);
			query.setParameter("email", value.getEmail()); 
			
			//2. query
			List<MemberEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			logging(e);
		}
		
		return result;
	}

	@Override
	public MemberEntity add(MemberEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			logging(e);
		}
		return value;
	}

	public int add(Register value) throws Exception {
		int id = 0;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Register.add");
			query.setParameter("firstName", value.getFirstName()); 
			query.setParameter("lastName", value.getLastName()); 
			query.setParameter("phone", value.getPhone()); 
			query.setParameter("email", value.getEmail()); 
			query.setParameter("password", value.getPassword()); 
			
			//2. query
			id = query.executeUpdate();
			
		}catch (Exception e) {
			logging(e);
		}
		
		return id;
	}
}