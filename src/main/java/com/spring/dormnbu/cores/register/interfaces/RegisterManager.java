package com.spring.dormnbu.cores.register.interfaces;

import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.register.domain.Register;

public interface RegisterManager {
	
	Member add(Register value) throws Exception;
}
