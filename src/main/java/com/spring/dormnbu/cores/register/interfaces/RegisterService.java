package com.spring.dormnbu.cores.register.interfaces;

import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.register.domain.Register;

public interface RegisterService {
	
	boolean checkDup(Register value) throws Exception;
	
	Member add(Register value) throws Exception;
}
