package com.spring.dormnbu.cores.register.domain;

import javax.validation.constraints.NotNull;

public class Register {
	
	@NotNull(message = "firstName not null")
	private String firstName;
	
	@NotNull(message = "lastName not null")
	private String lastName;
	
	@NotNull(message = "email not null")
	private String email;
	
	@NotNull(message = "password not null")
	private String password;
	
	@NotNull(message = "phone not null")
	private String phone;
	
	
	@Override
	public String toString() {
		return "Register [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", password="
				+ password + ", phone=" + phone + "]";
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
