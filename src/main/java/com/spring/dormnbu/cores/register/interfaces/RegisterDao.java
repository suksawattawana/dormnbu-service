package com.spring.dormnbu.cores.register.interfaces;

import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.register.domain.Register;

public interface RegisterDao {
	
	MemberEntity checkDup(Register value) throws Exception;
	
	MemberEntity add(MemberEntity value) throws Exception;
	
	int add(Register value) throws Exception;
}
