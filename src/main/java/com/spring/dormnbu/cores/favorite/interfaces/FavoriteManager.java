package com.spring.dormnbu.cores.favorite.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;

public interface FavoriteManager {

	Favorite add(Favorite value)  throws Exception;
	
	Favorite findById(int memberId) throws Exception;
	
	List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception;
	
	void delete(int id) throws Exception;
}
