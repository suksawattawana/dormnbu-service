package com.spring.dormnbu.cores.favorite.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.entity.DormEntity;
import com.spring.dormnbu.cores.entity.FavoriteEntity;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteDao;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteService;
import com.spring.dormnbu.cores.member.interfaces.MemberDao;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class FavoriteServiceImp implements FavoriteService {

	@Autowired
	private FavoriteDao favDao;
	
	@Autowired
	private DormDao dormDao;
	
	@Autowired
	private MemberDao memberDao;

	@Override
	public boolean checkDup(Favorite value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			FavoriteEntity data = favDao.checkDup(value);
			
			//2. ตรวจสอบว่ามีในฐานข้อมูลมั้ย
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Favorite add(Favorite value) throws Exception {
		Favorite result = null;
		
		//1. ค้นหาข้อมูล
		MemberEntity member = memberDao.findById(value.getMemberId());
		DormEntity dorm = dormDao.findById(value.getDormId());
		
		try {
			//2. ตรวจสอบว่ามีข้อมูลหรือไม่
			if (member != null && dorm != null){
				
				//3. จัดกาข้อมูล
				FavoriteEntity data = new FavoriteEntity();
				BeanUtils.copyProperties(data, value);
				data.setCreateDate(DateTimeUtil.getDateCurentDate());
				
				//4. เพิ่มข้อมูล
				data = favDao.add(data);
				
				//5. return ข้อมุล
				result = new Favorite();
				BeanUtils.copyProperties(result, data);
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
			}
			else{
				throw new DataNotFoundException("Member or Dorm Data not Found!");
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		favDao.delete(id);
	}
	
	@Override
	public Favorite findById(int id) throws Exception {
		Favorite result = null;
		
		try {
			//1. ค้นหาข้อมูล
			FavoriteEntity data = favDao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Favorite();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
				
				//2.2 ข้อมูลเพิ่ม
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception {
		List<Favorite> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<Favorite> datas = favDao.findByCriteria(criteria);
			
			//2. จัดการผลลัพธ์
			if (datas != null){
				
				result = new ArrayList<Favorite>();
				for (Favorite data : datas) {
					Favorite favorite = new Favorite();
					BeanUtils.copyProperties(favorite, data);
					result.add(favorite);
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
}
