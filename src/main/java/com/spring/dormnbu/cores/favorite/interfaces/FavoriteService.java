package com.spring.dormnbu.cores.favorite.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;

public interface FavoriteService {

	boolean checkDup(Favorite value) throws Exception;
	
	Favorite add(Favorite value)  throws Exception;
	
	void delete(int id) throws Exception;
	
	Favorite findById(int id) throws Exception;
	
	List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception;
	
}
