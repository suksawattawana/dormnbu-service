package com.spring.dormnbu.cores.favorite.domain;

import com.spring.dormnbu.commons.CommonSearchCriteria;

public class FavoriteCriteria extends CommonSearchCriteria {

	private static final long serialVersionUID = -4654714813298295450L;
	private int memberId;
	
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	
	
}
