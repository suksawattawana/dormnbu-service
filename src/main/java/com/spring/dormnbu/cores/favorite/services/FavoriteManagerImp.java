package com.spring.dormnbu.cores.favorite.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteManager;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteService;

@Component
@Transactional
@Scope("request")
public class FavoriteManagerImp implements FavoriteManager {

	@Autowired
	private FavoriteService service;

	@Override
	public Favorite add(Favorite value) throws Exception {
		Favorite result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null) {
				
				//2. ตรวจสอบข้อมูลซ้ำ
				boolean checkDup = service.checkDup(value);
				
				if (checkDup == false){
					result = service.add(value);
				}
				else{
					throw new DuplicateException();
				}
			}
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ตรวจสอบว่ามีข้อมูลหรือไม่
				Favorite data = service.findById(id);
				
				if(data != null){
					//2. ลบข้อมูล
					service.delete(id);
				}
				else{
					throw new DataNotFoundException("No Data for key: " + id);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception {
		List<Favorite> result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (criteria != null) {
				
				//2. ค้นหาข้อมูล
				result = service.findByCriteria(criteria);
			}
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Favorite findById(int id) throws Exception {
		Favorite result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findById(id);
			}
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
