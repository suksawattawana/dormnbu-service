package com.spring.dormnbu.cores.favorite.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.entity.FavoriteEntity;
import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;

public interface FavoriteDao {

	FavoriteEntity checkDup(Favorite value) throws Exception;
	
	FavoriteEntity add(FavoriteEntity value)  throws Exception;
	
	void delete(int id) throws Exception;
	
	FavoriteEntity findById(int id) throws Exception;
	
	List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception;
	
}
