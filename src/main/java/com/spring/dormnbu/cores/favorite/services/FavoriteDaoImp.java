package com.spring.dormnbu.cores.favorite.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.sql.IgnoreCaseAliasToBeanResultTransformer;
import com.spring.dormnbu.cores.entity.FavoriteEntity;
import com.spring.dormnbu.cores.favorite.domain.Favorite;
import com.spring.dormnbu.cores.favorite.domain.FavoriteCriteria;
import com.spring.dormnbu.cores.favorite.interfaces.FavoriteDao;

@Repository
@Transactional
@Scope("request")
public class FavoriteDaoImp implements FavoriteDao{

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public FavoriteEntity checkDup(Favorite value) throws Exception {
		FavoriteEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Favorite.checkDup", FavoriteEntity.class);
			query.setParameter("memberId", value.getMemberId()); 
			query.setParameter("dormId", value.getDormId()); 
			
			//2. query
			List<FavoriteEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public FavoriteEntity add(FavoriteEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Favorite> findByCriteria(FavoriteCriteria criteria) throws Exception {
		List<Favorite> result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Favorite.findByCriteria");
			query.setParameter("memberId", criteria.getMemberId());
			
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(Favorite.class));
			
			//2. query
			result = query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. map sql 
			Query query = em.createNamedQuery("Favorite.delete");
			query.setParameter("favoriteId", id);
			
			//2. query
			query.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public FavoriteEntity findById(int id) throws Exception {
		FavoriteEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Favorite.findById",FavoriteEntity.class);
			query.setParameter("favoriteId", id);
			
			//2. query
			List<FavoriteEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
}
