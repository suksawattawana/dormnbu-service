package com.spring.dormnbu.cores.member.domain;

import com.spring.dormnbu.commons.CommonSearchCriteria;

public class CriteriaMember extends CommonSearchCriteria {

	private static final long serialVersionUID = -2107326800923670792L;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
