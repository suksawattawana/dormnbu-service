package com.spring.dormnbu.cores.member.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;
import com.spring.dormnbu.cores.member.interfaces.MemberDao;
import com.spring.dormnbu.cores.member.interfaces.MemberService;
import com.spring.dormnbu.enums.MemberType;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class MemberServiceImp implements MemberService {

	@Autowired
	private MemberDao dao;
	
	@Override
	public boolean checkDup(Member value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			MemberEntity data = dao.checkDup(value);
			
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public Member add(Member value) throws Exception {
		Member result = null;
		
		try {
			//2 จัดกาข้อมูล
			MemberEntity data = new MemberEntity();
			BeanUtils.copyProperties(data, value);
			data.setMemberType(MemberType.Member.getKey().charAt(0));
			data.setStatus('Y');
			data.setCreateDate(DateTimeUtil.getDateCurentDate());
			
			//3. อัพเดทข้อมูล
			dao.add(data);
			
			//4. return ข้อมุล
			result = new Member();
			BeanUtils.copyProperties(result, data);
			result.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
			result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
			result.setModify(DateTimeUtil.dateToString(data.getModify()));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public Member edit(Member value) throws Exception {
		Member result = null;
		
		try {
			//1. ค้นหาข้อมูล
			MemberEntity data = dao.findById(value.getMemberId());
			
			//2. จัดการผลลัพธ์
			if (data != null){
				
				//2.1 ข้อมูลที่มีการแก้ไข
				if (value.getFirstName() != null){
					data.setFirstName(value.getFirstName());
				}
				if (value.getLastName() != null){
					data.setLastName(value.getLastName());
				}
				if (value.getPhone() != null){
					data.setPhone(value.getPhone());
				}
				data.setModify(DateTimeUtil.getDateCurentDate());
				
				//3. อัพเดทข้อมูล
				dao.edit(data);
				
				//4. return ข้อมุล
				result = new Member();
				BeanUtils.copyProperties(result, data);
				result.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public String updateMemberType(int id) throws Exception {
		return null;
	}
	
	@Override
	public String updateStatus(int id) throws Exception {
		return null;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ค้นหาข้อมูล
			MemberEntity entity = dao.findById(id);
			
			//2. ลบข้อมูล
			if (entity != null){
				dao.delete(entity);
			}
			else{
				throw new DataNotFoundException("No Data for key: " + id);
			}
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Member findById(int id) throws Exception {
		Member result = null;
		
		try {
			//1. ค้นหาข้อมูล
			MemberEntity data = dao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Member();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
				
				//2.2 ข้อมูลเพิ่ม
				result.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public List<Member> findByCriteria(CriteriaMember criteria) throws Exception {
		List<Member> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<Member> datas = dao.findByCriteria(criteria);
			
			//2. จัดการผลลัพธ์
			if (datas != null){
				
				result = new ArrayList<Member>();
				for (Member data : datas) {
					Member member = new Member();
					BeanUtils.copyProperties(member, data);
					member.setMemberTypeDesc(MemberType.findValue(data.getMemberType().toString()));
					member.setCreateDate(data.getCreateDate());
					member.setModify(data.getModify());
					result.add(member);
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
