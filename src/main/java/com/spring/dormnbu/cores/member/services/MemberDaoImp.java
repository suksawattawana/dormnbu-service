package com.spring.dormnbu.cores.member.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.sql.IgnoreCaseAliasToBeanResultTransformer;
import com.spring.dormnbu.commons.sql.SQLUtils;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;
import com.spring.dormnbu.cores.member.interfaces.MemberDao;

@Repository
@Transactional
@Scope("request")
public class MemberDaoImp implements MemberDao{

	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Override
	public MemberEntity checkDup(Member value) throws Exception {
		MemberEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Member.checkDup", MemberEntity.class);
			query.setParameter("email", value.getEmail()); 
			
			//2. query
			List<MemberEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public MemberEntity findById(int id) throws Exception {
		MemberEntity result = null;
		
		try{
			result = em.find(MemberEntity.class, id);
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Member> findByCriteria(CriteriaMember criteria) throws Exception {
		List<Member> result = null;
		
		try{
			//1. map sql
			Query query = SQLUtils.getQuery(em, "Member.findByCriteria", criteria);
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(Member.class));
			
			//2. query
			result =  query.getResultList();
			
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}

	@Override
	public void add(MemberEntity value) throws Exception {
		try{
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void delete(MemberEntity value) throws Exception {
		
		try{
			em.remove(value);
		}catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public MemberEntity edit(MemberEntity value) throws Exception {
		try{
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public void updateMemberType(int memberId, String memberType) throws Exception {
		try{
			//1. map sql
			Query query = em.createNamedQuery("Member.updateMemberType");
			query.setParameter("memberId", memberId);
			query.setParameter("memberType", memberType);
			
			//2. query
			query.executeUpdate();
			
		}catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void updateStatus(int memberId, String status) throws Exception {
		try{
			//1. map sql
			Query query = em.createNamedQuery("Member.updateStatus");
			query.setParameter("memberId", memberId);
			query.setParameter("status", status);
			
			//2. query
			query.executeUpdate();
			
		}catch (Exception e) {
			throw e;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer checkNullMember(int id) throws Exception {
		Integer result = 0;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Member.checkNullMember");
			query.setParameter("memberId", id); 
			
			//2. query
			List<Integer> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
