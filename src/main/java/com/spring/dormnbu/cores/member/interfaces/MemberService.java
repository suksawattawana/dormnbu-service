package com.spring.dormnbu.cores.member.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;

public interface MemberService {

	boolean checkDup(Member value) throws Exception;
	
	Member findById(int id) throws Exception;
	
	List<Member> findByCriteria(CriteriaMember criteria) throws Exception;
	
	Member add(Member value) throws Exception;
	
	Member edit(Member value) throws Exception;
	
	String updateMemberType(int id) throws Exception;
	
	String updateStatus(int id) throws Exception;
	
	void delete(int id) throws Exception;
}
