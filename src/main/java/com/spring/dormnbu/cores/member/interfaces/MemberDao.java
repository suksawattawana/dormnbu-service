package com.spring.dormnbu.cores.member.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.entity.MemberEntity;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;

public interface MemberDao {

	MemberEntity checkDup(Member value) throws Exception;
	
	Integer checkNullMember(int id) throws Exception;
	
	MemberEntity findById(int id) throws Exception;
	
	List<Member> findByCriteria(CriteriaMember criteria) throws Exception;
	
	void add(MemberEntity value) throws Exception;
	
	MemberEntity edit(MemberEntity value) throws Exception;
	
	void updateMemberType(int id, String value) throws Exception;
	
	void updateStatus(int id, String value) throws Exception;
	
	void delete(MemberEntity value) throws Exception;
}
