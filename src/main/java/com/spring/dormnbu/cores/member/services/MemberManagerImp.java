package com.spring.dormnbu.cores.member.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.cores.domain.Member;
import com.spring.dormnbu.cores.member.domain.CriteriaMember;
import com.spring.dormnbu.cores.member.interfaces.MemberManager;
import com.spring.dormnbu.cores.member.interfaces.MemberService;

@Component
@Transactional
@Scope("request")
public class MemberManagerImp implements MemberManager {

	@Autowired
	private MemberService service;
	
	@Override
	public Member add(Member value) throws Exception {
		Member result = null;
		
		//1. ตรวจสอบ criteria
		if (value != null) {
			
			//2. ตรวจสอบข้อมูลซ้ำ
			boolean checkDup = service.checkDup(value);
			
			if (!checkDup){
				result = service.add(value);
			}
			else{
				throw new DuplicateException("This email is already in use.");
			}
		}
		
		return result;
	}

	@Override
	public Member edit(Member value) throws Exception {
		Member result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getMemberId() != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.edit(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ลบข้อมูล
				service.delete(id);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Member findById(int id) throws Exception {
		Member result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findById(id);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public List<Member> findByCriteria(CriteriaMember criteria) throws Exception {
		List<Member> result = null;
		
		try{
			//1. ตรวจสอบ criteria
			if (criteria != null) {
				
				//2. ค้นหาข้อมูล
				result = service.findByCriteria(criteria);
			}
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
