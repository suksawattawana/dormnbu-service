package com.spring.dormnbu.cores.comment.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.comment.domain.Comment;
import com.spring.dormnbu.cores.comment.interfaces.CommentManager;
import com.spring.dormnbu.cores.comment.interfaces.CommentService;

@Component
@Transactional
@Scope("request")
public class CommentManagerImp implements CommentManager {
	
	@Autowired
	private CommentService service;
	
	public Comment add(Comment value) throws Exception {
		Comment result = null;

		try {
			//1. ตรวจสอบ criteria
			if (value != null) {
				
				//2. ตรวจสอบค่าว่าง
				boolean checkNull = service.checkNullData(value);
				if (checkNull){
					result = service.add(value);
				}
				else{
					throw new DataNotFoundException("Data Not Found!");
				}
				
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Comment edit(Comment value) throws Exception {
		Comment result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getCommentId() != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.edit(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Comment findById(int id) throws Exception {
		Comment result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findById(id);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ลบข้อมูล
				service.delete(id);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<Comment> findByDormId(int dormId) throws Exception {
		List<Comment> result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (dormId != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findByDormId(dormId);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
}
