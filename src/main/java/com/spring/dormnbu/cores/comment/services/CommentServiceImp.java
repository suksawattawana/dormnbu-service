package com.spring.dormnbu.cores.comment.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.comment.domain.Comment;
import com.spring.dormnbu.cores.comment.interfaces.CommentDao;
import com.spring.dormnbu.cores.comment.interfaces.CommentService;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.entity.CommentEntity;
import com.spring.dormnbu.cores.member.interfaces.MemberDao;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class CommentServiceImp implements CommentService {
	
	@Autowired
	private CommentDao dao;
	
	@Autowired
	private DormDao dormDao;
	
	@Autowired
	private MemberDao memberDao;

	@Override
	public boolean checkDup(Comment value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			CommentEntity data = dao.checkDup(value);
			
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public boolean checkNullData(Comment value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			Integer memberId =  memberDao.checkNullMember(value.getMemberId());
			Integer dormId = dormDao.checkNullDorm(value.getDormId());
			
			if (memberId != 0 && dormId != 0){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public Comment add(Comment value) throws Exception {
		Comment result = null;
		
		try {
			//1. จัดกาข้อมูล
			CommentEntity data = new CommentEntity();
			BeanUtils.copyProperties(data, value);
			data.setCreateDate(DateTimeUtil.getDateCurentDate());
				
			//2. อัพเดทข้อมูล
			data = dao.add(data);
				
			//3. return ข้อมุล
			result = new Comment();
			BeanUtils.copyProperties(result, data);
			result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		return result;
	}

	@Override
	public Comment edit(Comment value) throws Exception {
		Comment result = null;
		
		try {
			//1. ค้นหาข้อมูล
			CommentEntity data = dao.findById(value.getCommentId());
			
			//2. จัดการผลลัพธ์
			if (data != null){
				
				//2.1 ข้อมูลที่มีการแก้ไข
				data.setDetail(value.getDetail());
				data.setModify(DateTimeUtil.getDateCurentDate());
				
				//3. อัพเดทข้อมูล
				dao.edit(data);
				
				//4. return ข้อมุล
				result = new Comment();
				BeanUtils.copyProperties(result, data);
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public Comment findById(int id) throws Exception {
		Comment result = null;
		
		try {
			//1. ค้นหาข้อมูล
			CommentEntity data = dao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Comment();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
				
				//2.2 ข้อมูลเพิ่มเติม
				result.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
				result.setModify(DateTimeUtil.dateToString(data.getModify()));
			}
			
		} catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ค้นหาข้อมูล
			CommentEntity entity = dao.findById(id);
			
			//2. ลบข้อมูล
			if (entity != null){
				dao.delete(entity);
			}
			else{
				throw new DataNotFoundException("No Data for key: " + id);
			}
			
		} catch (Exception e) {
			throw e;
		}		
	}

	@Override
	public List<Comment> findByDormId(int dormId) throws Exception {
		List<Comment> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<CommentEntity> datas = dao.findByDormId(dormId);
			
			//2. จัดการผลลัพธ์
			if (datas != null){
				
				result = new ArrayList<Comment>();
				for (CommentEntity data : datas) {
					Comment comment = new Comment();
					BeanUtils.copyProperties(comment, data);
					comment.setCreateDate(DateTimeUtil.dateToString(data.getCreateDate()));
					comment.setModify(DateTimeUtil.dateToString(data.getModify()));
					result.add(comment);
				}
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
}
