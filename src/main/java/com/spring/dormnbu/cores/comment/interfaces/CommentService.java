package com.spring.dormnbu.cores.comment.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.comment.domain.Comment;

public interface CommentService {

	boolean checkDup(Comment value) throws Exception;
	
	boolean checkNullData(Comment value) throws Exception;
	
	Comment add(Comment value) throws Exception;
	
	Comment edit(Comment value) throws Exception;
	
	void delete(int id) throws Exception;
	
	Comment findById(int id) throws Exception;
	
	List<Comment> findByDormId(int dormId) throws Exception;
	
}
