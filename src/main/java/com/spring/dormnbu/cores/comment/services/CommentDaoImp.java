package com.spring.dormnbu.cores.comment.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.comment.domain.Comment;
import com.spring.dormnbu.cores.comment.interfaces.CommentDao;
import com.spring.dormnbu.cores.entity.CommentEntity;

@Repository
@Transactional
@Scope("request")
public class CommentDaoImp implements CommentDao{
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public CommentEntity add(CommentEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public CommentEntity edit(CommentEntity value) throws Exception {
		try{
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public void delete(CommentEntity value) throws Exception {
		try{
			em.remove(value);
		}catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public CommentEntity findById(int id) throws Exception {
		CommentEntity result = null;
		
		try{
			result = em.find(CommentEntity.class, id);	
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CommentEntity checkDup(Comment value) throws Exception {
		CommentEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Comment.checkDup", CommentEntity.class);
			query.setParameter("dormId", value.getDormId()); 
			query.setParameter("memberId", value.getMemberId()); 
			
			//2. query
			List<CommentEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CommentEntity> findByDormId(int dormId) throws Exception {
		List<CommentEntity> result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Comment.findByDormId", CommentEntity.class);
			query.setParameter("dormId", dormId); 
			
			//2. query
			result = query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
