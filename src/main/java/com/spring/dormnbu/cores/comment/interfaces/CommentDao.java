package com.spring.dormnbu.cores.comment.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.comment.domain.Comment;
import com.spring.dormnbu.cores.entity.CommentEntity;

public interface CommentDao {
	
	CommentEntity checkDup(Comment value) throws Exception;
	
	CommentEntity add(CommentEntity value) throws Exception;
	
	CommentEntity edit(CommentEntity value) throws Exception;
	
	void delete(CommentEntity value) throws Exception;
	
	CommentEntity findById(int id) throws Exception;
	
	List<CommentEntity> findByDormId(int dormId) throws Exception;
}
