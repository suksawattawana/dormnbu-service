package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the dorm database table.
 * 
 */
@Entity
@Table(name = "dorm")
public class DormEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int dormId;
	private String dormCode;
	private int memberId;
	
	private String nameTh;
	private String nameEn;
	private String phone1;
	private String phone2;
	private String email;
	private String line;
	private String detail;
	
	private Character waterType;
	private int waterMax;
	private int waterMin;
	private int waterPrice;
	
	private Character electricityType;
	private int electricityMax;
	private int electricityMin;
	private int electricityPrice;
	
	private Character otherServiceType;
	private String otherServiceDesc;
	private int otherServicePrice;
	
	private Character internetType;
	private int internetPrice;
	private Character depositType;
	private int depositPrice;
	private Character payadvanceType;
	private int payadvancePrice;
	
	private Date createDate;
	private Date modify;
	
	private AmenitieEntity amenitie;
	private AreaEntity area;
	private PictureEntity picture;
	private PromotionEntity promotion;
	private List<RoomtypeEntity> roomtypes;

	public DormEntity() {
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dorm_id")
	public int getDormId() {
		return this.dormId;
	}

	public void setDormId(int dormId) {
		this.dormId = dormId;
	}


	@Column(name="deposit_price")
	public int getDepositPrice() {
		return this.depositPrice;
	}

	public void setDepositPrice(int depositPrice) {
		this.depositPrice = depositPrice;
	}


	@Column(name="deposit_type")
	public Character getDepositType() {
		return this.depositType;
	}

	public void setDepositType(Character depositType) {
		this.depositType = depositType;
	}


	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}


	@Column(name="dorm_code")
	public String getDormCode() {
		return this.dormCode;
	}

	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}


	@Column(name="electricity_max")
	public int getElectricityMax() {
		return this.electricityMax;
	}

	public void setElectricityMax(int electricityMax) {
		this.electricityMax = electricityMax;
	}


	@Column(name="electricity_min")
	public int getElectricityMin() {
		return this.electricityMin;
	}

	public void setElectricityMin(int electricityMin) {
		this.electricityMin = electricityMin;
	}


	@Column(name="electricity_price")
	public int getElectricityPrice() {
		return this.electricityPrice;
	}

	public void setElectricityPrice(int electricityPrice) {
		this.electricityPrice = electricityPrice;
	}


	@Column(name="electricity_type")
	public Character getElectricityType() {
		return this.electricityType;
	}

	public void setElectricityType(Character electricityType) {
		this.electricityType = electricityType;
	}


	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name="internet_price")
	public int getInternetPrice() {
		return this.internetPrice;
	}

	public void setInternetPrice(int internetPrice) {
		this.internetPrice = internetPrice;
	}


	@Column(name="internet_type")
	public Character getInternetType() {
		return this.internetType;
	}

	public void setInternetType(Character internetType) {
		this.internetType = internetType;
	}


	public String getLine() {
		return this.line;
	}

	public void setLine(String line) {
		this.line = line;
	}


	@Column(name="member_id")
	public int getMemberId() {
		return this.memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}


	@Column(name="name_en")
	public String getNameEn() {
		return this.nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}


	@Column(name="name_th")
	public String getNameTh() {
		return this.nameTh;
	}

	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}


	@Column(name="other_service_desc")
	public String getOtherServiceDesc() {
		return this.otherServiceDesc;
	}

	public void setOtherServiceDesc(String otherServiceDesc) {
		this.otherServiceDesc = otherServiceDesc;
	}


	@Column(name="other_service_price")
	public int getOtherServicePrice() {
		return this.otherServicePrice;
	}

	public void setOtherServicePrice(int otherServicePrice) {
		this.otherServicePrice = otherServicePrice;
	}


	@Column(name="other_service_type")
	public Character getOtherServiceType() {
		return this.otherServiceType;
	}

	public void setOtherServiceType(Character otherServiceType) {
		this.otherServiceType = otherServiceType;
	}


	@Column(name="payadvance_price")
	public int getPayadvancePrice() {
		return this.payadvancePrice;
	}

	public void setPayadvancePrice(int payadvancePrice) {
		this.payadvancePrice = payadvancePrice;
	}


	@Column(name="payadvance_type")
	public Character getPayadvanceType() {
		return this.payadvanceType;
	}

	public void setPayadvanceType(Character payadvanceType) {
		this.payadvanceType = payadvanceType;
	}


	public String getPhone1() {
		return this.phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}


	public String getPhone2() {
		return this.phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}


	@Column(name="water_max")
	public int getWaterMax() {
		return this.waterMax;
	}

	public void setWaterMax(int waterMax) {
		this.waterMax = waterMax;
	}


	@Column(name="water_min")
	public int getWaterMin() {
		return this.waterMin;
	}

	public void setWaterMin(int waterMin) {
		this.waterMin = waterMin;
	}


	@Column(name="water_price")
	public int getWaterPrice() {
		return this.waterPrice;
	}

	public void setWaterPrice(int waterPrice) {
		this.waterPrice = waterPrice;
	}


	@Column(name="water_type")
	public Character getWaterType() {
		return this.waterType;
	}

	public void setWaterType(Character waterType) {
		this.waterType = waterType;
	}
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	public Date getCreateDate() {
		return this.createDate;
	}
	
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getModify() {
		return this.modify;
	}
	
	public void setModify(Date modify) {
		this.modify = modify;
	}


	//bi-directional one-to-one association to Amenitie
	@OneToOne( fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "amenitie_id")
	public AmenitieEntity getAmenitie() {
		return this.amenitie;
	}

	public void setAmenitie(AmenitieEntity amenitie) {
		this.amenitie = amenitie;
	}


	//bi-directional one-to-one association to Area
	@OneToOne( fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "area_id")
	public AreaEntity getArea() {
		return this.area;
	}

	public void setArea(AreaEntity area) {
		this.area = area;
	}


	//bi-directional one-to-one association to Picture
	@OneToOne( fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "picture_id")
	public PictureEntity getPicture() {
		return this.picture;
	}

	public void setPicture(PictureEntity picture) {
		this.picture = picture;
	}


	//bi-directional one-to-one association to Promotion
	@OneToOne( fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name = "promotion_id")
	public PromotionEntity getPromotion() {
		return this.promotion;
	}

	public void setPromotion(PromotionEntity promotion) {
		this.promotion = promotion;
	}


	//bi-directional many-to-one association to Roomtype
	@OneToMany(mappedBy="dorm", cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH, CascadeType.REMOVE})
	public List<RoomtypeEntity> getRoomtypes() {
		return this.roomtypes;
	}

	public void setRoomtypes(List<RoomtypeEntity> roomtypes) {
		this.roomtypes = roomtypes;
	}

	public RoomtypeEntity addRoomtype(RoomtypeEntity roomtype) {
		
		if( roomtypes == null){
			roomtypes = new ArrayList<>();
		}
		
		getRoomtypes().add(roomtype);
		roomtype.setDorm(this);

		return roomtype;
	}

	public RoomtypeEntity removeRoomtype(RoomtypeEntity roomtype) {
		getRoomtypes().remove(roomtype);
		roomtype.setDorm(null);

		return roomtype;
	}

}