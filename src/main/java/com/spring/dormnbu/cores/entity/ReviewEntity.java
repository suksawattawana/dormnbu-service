package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the review database table.
 * 
 */
@Entity
@Table(name="review")
public class ReviewEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int reviewId;
	private int dormId;
	private int memberId;
	private int score;
	private Date createDate;
	private Date modify;

	public ReviewEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="review_id")
	public int getReviewId() {
		return this.reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Column(name="dorm_id")
	public int getDormId() {
		return this.dormId;
	}

	public void setDormId(int dormId) {
		this.dormId = dormId;
	}


	@Column(name="member_id")
	public int getMemberId() {
		return this.memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getModify() {
		return this.modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}


	public int getScore() {
		return this.score;
	}

	public void setScore(int score) {
		this.score = score;
	}

}