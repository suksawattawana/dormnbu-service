package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the global_electricity database table.
 * 
 */
@Entity
@Table(name="global_electricity")
public class GlobalElectricityEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int electricityId;
	private String electricityDesc;
	private Character electricityType;

	public GlobalElectricityEntity() {
	}


	@Id
	@Column(name="electricity_id")
	public int getElectricityId() {
		return this.electricityId;
	}

	public void setElectricityId(int electricityId) {
		this.electricityId = electricityId;
	}


	@Column(name="electricity_desc")
	public String getElectricityDesc() {
		return this.electricityDesc;
	}

	public void setElectricityDesc(String electricityDesc) {
		this.electricityDesc = electricityDesc;
	}


	@Column(name="electricity_type")
	public Character getElectricityType() {
		return this.electricityType;
	}

	public void setElectricityType(Character electricityType) {
		this.electricityType = electricityType;
	}

}