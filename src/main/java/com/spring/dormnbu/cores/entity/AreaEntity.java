package com.spring.dormnbu.cores.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the area database table.
 * 
 */
@Entity
@Table(name = "area")
public class AreaEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int areaId;
	private String no;
	private String road;
	private String soi;
	
	public AreaEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="area_id")
	public int getAreaId() {
		return this.areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}


	public String getNo() {
		return this.no;
	}

	public void setNo(String no) {
		this.no = no;
	}


	public String getRoad() {
		return this.road;
	}

	public void setRoad(String road) {
		this.road = road;
	}


	public String getSoi() {
		return this.soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}
}