package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the comment database table.
 * 
 */
@Entity
@Table(name = "comment")
public class CommentEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int commentId;
	private int dormId;
	private int memberId;
	private String detail;
	private Date createDate;
	private Date modify;

	public CommentEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="comment_id")
	public int getCommentId() {
		return this.commentId;
	}

	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getDetail() {
		return this.detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}


	@Column(name="dorm_id")
	public int getDormId() {
		return this.dormId;
	}

	public void setDormId(int dormId) {
		this.dormId = dormId;
	}


	@Column(name="member_id")
	public int getMemberId() {
		return this.memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getModify() {
		return this.modify;
	}

	public void setModify(Date modify) {
		this.modify = modify;
	}

}