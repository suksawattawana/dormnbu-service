package com.spring.dormnbu.cores.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the amenitie database table.
 * 
 */
@Entity
@Table(name = "amenitie")
public class AmenitieEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int amenitieId;
	private Character air;
	private Character cable;
	private Character cctv;
	private Character fan;
	private Character furniture;
	private Character gymnasium;
	private Character hairSalon;
	private Character keycard;
	private Character lift;
	private Character washing;
	private Character waterHeater;
	private Character wifi;
	
	public AmenitieEntity() {
		
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="amenitie_id")
	public int getAmenitieId() {
		return this.amenitieId;
	}

	public void setAmenitieId(int amenitieId) {
		this.amenitieId = amenitieId;
	}


	public Character getAir() {
		return this.air;
	}

	public void setAir(Character air) {
		this.air = air;
	}


	public Character getCable() {
		return this.cable;
	}

	public void setCable(Character cable) {
		this.cable = cable;
	}


	public Character getCctv() {
		return this.cctv;
	}

	public void setCctv(Character cctv) {
		this.cctv = cctv;
	}


	public Character getFan() {
		return this.fan;
	}

	public void setFan(Character fan) {
		this.fan = fan;
	}


	public Character getFurniture() {
		return this.furniture;
	}

	public void setFurniture(Character furniture) {
		this.furniture = furniture;
	}


	public Character getGymnasium() {
		return this.gymnasium;
	}

	public void setGymnasium(Character gymnasium) {
		this.gymnasium = gymnasium;
	}


	@Column(name="hair_salon")
	public Character getHairSalon() {
		return this.hairSalon;
	}

	public void setHairSalon(Character hairSalon) {
		this.hairSalon = hairSalon;
	}


	public Character getKeycard() {
		return this.keycard;
	}

	public void setKeycard(Character keycard) {
		this.keycard = keycard;
	}


	public Character getLift() {
		return this.lift;
	}

	public void setLift(Character lift) {
		this.lift = lift;
	}


	public Character getWashing() {
		return this.washing;
	}

	public void setWashing(Character washing) {
		this.washing = washing;
	}


	@Column(name="water_heater")
	public Character getWaterHeater() {
		return this.waterHeater;
	}

	public void setWaterHeater(Character waterHeater) {
		this.waterHeater = waterHeater;
	}


	public Character getWifi() {
		return this.wifi;
	}

	public void setWifi(Character wifi) {
		this.wifi = wifi;
	}

}