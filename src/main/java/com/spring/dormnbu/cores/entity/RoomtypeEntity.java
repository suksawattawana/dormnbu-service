package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the roomtype database table.
 * 
 */
@Entity
@Table(name="roomtype")
public class RoomtypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int roomtypeId;
	private String type;
	private String format;
	private String size;
	private String unit;
	private Character status;
	
	private Character monthlyRent;
	private int monthlyMax;
	private int monthlyMin;
	
	private Character dailyRent;
	private int dailyMax;
	private int dailyMin;
	
	private DormEntity dorm;

	public RoomtypeEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="roomtype_id")
	public int getRoomtypeId() {
		return this.roomtypeId;
	}

	public void setRoomtypeId(int roomtypeId) {
		this.roomtypeId = roomtypeId;
	}


	@Column(name="daily_max")
	public int getDailyMax() {
		return this.dailyMax;
	}

	public void setDailyMax(int dailyMax) {
		this.dailyMax = dailyMax;
	}


	@Column(name="daily_min")
	public int getDailyMin() {
		return this.dailyMin;
	}

	public void setDailyMin(int dailyMin) {
		this.dailyMin = dailyMin;
	}


	@Column(name="daily_rent")
	public Character getDailyRent() {
		return this.dailyRent;
	}

	public void setDailyRent(Character dailyRent) {
		this.dailyRent = dailyRent;
	}


	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}


	@Column(name="monthly_max")
	public int getMonthlyMax() {
		return this.monthlyMax;
	}

	public void setMonthlyMax(int monthlyMax) {
		this.monthlyMax = monthlyMax;
	}


	@Column(name="monthly_min")
	public int getMonthlyMin() {
		return this.monthlyMin;
	}

	public void setMonthlyMin(int monthlyMin) {
		this.monthlyMin = monthlyMin;
	}


	@Column(name="monthly_rent")
	public Character getMonthlyRent() {
		return this.monthlyRent;
	}

	public void setMonthlyRent(Character monthlyRent) {
		this.monthlyRent = monthlyRent;
	}


	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}


	public Character getStatus() {
		return this.status;
	}

	public void setStatus(Character status) {
		this.status = status;
	}


	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}


	//bi-directional many-to-one association to Dorm
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="dorm_id")
	public DormEntity getDorm() {
		return this.dorm;
	}

	public void setDorm(DormEntity dorm) {
		this.dorm = dorm;
	}

}