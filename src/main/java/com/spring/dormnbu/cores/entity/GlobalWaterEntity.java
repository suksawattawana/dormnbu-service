package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the global_water database table.
 * 
 */
@Entity
@Table(name="global_water")
public class GlobalWaterEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int waterId;
	private String waterDesc;
	private Character waterType;

	public GlobalWaterEntity() {
	}


	@Id
	@Column(name="water_id")
	public int getWaterId() {
		return this.waterId;
	}

	public void setWaterId(int waterId) {
		this.waterId = waterId;
	}


	@Column(name="water_desc")
	public String getWaterDesc() {
		return this.waterDesc;
	}

	public void setWaterDesc(String waterDesc) {
		this.waterDesc = waterDesc;
	}


	@Column(name="water_type")
	public Character getWaterType() {
		return this.waterType;
	}

	public void setWaterType(Character waterType) {
		this.waterType = waterType;
	}

}