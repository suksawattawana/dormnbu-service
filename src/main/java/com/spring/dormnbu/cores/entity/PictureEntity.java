package com.spring.dormnbu.cores.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * The persistent class for the picture database table.
 * 
 */
@Entity
@Table(name="picture")
public class PictureEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int pictureId;
	private String picture1;
	private String picture2;
	private String picture3;
	private String picture4;
	private String picture5;
	private String picture6;
	private String picture7;
	private String picture8;
	private String picture9;
	private String picture10;
	
	public PictureEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="picture_id")
	public int getPictureId() {
		return this.pictureId;
	}

	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}


	@Column(name="picture_1")
	public String getPicture1() {
		return this.picture1;
	}

	public void setPicture1(String picture1) {
		this.picture1 = picture1;
	}


	@Column(name="picture_2")
	public String getPicture2() {
		return this.picture2;
	}

	public void setPicture2(String picture2) {
		this.picture2 = picture2;
	}


	@Column(name="picture_3")
	public String getPicture3() {
		return this.picture3;
	}

	public void setPicture3(String picture3) {
		this.picture3 = picture3;
	}


	@Column(name="picture_4")
	public String getPicture4() {
		return this.picture4;
	}

	public void setPicture4(String picture4) {
		this.picture4 = picture4;
	}


	@Column(name="picture_5")
	public String getPicture5() {
		return this.picture5;
	}

	public void setPicture5(String picture5) {
		this.picture5 = picture5;
	}


	@Column(name="picture_6")
	public String getPicture6() {
		return this.picture6;
	}

	public void setPicture6(String picture6) {
		this.picture6 = picture6;
	}


	@Column(name="picture_7")
	public String getPicture7() {
		return this.picture7;
	}

	public void setPicture7(String picture7) {
		this.picture7 = picture7;
	}

	
	@Column(name="picture_8")
	public String getPicture8() {
		return this.picture8;
	}

	public void setPicture8(String picture8) {
		this.picture8 = picture8;
	}

	
	@Column(name="picture_9")
	public String getPicture9() {
		return this.picture9;
	}

	public void setPicture9(String picture9) {
		this.picture9 = picture9;
	}
	
	
	@Column(name="picture_10")
	public String getPicture10() {
		return this.picture10;
	}

	public void setPicture10(String picture10) {
		this.picture10 = picture10;
	}
}