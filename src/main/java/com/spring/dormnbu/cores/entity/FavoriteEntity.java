package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the favorite database table.
 * 
 */
@Entity
@Table(name = "favorite")
public class FavoriteEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private int favoriteId;
	private Date createDate;
	private int dormId;
	private int memberId;

	public FavoriteEntity() {
		
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="favorite_id")
	public int getFavoriteId() {
		return this.favoriteId;
	}

	public void setFavoriteId(int favoriteId) {
		this.favoriteId = favoriteId;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	public Date getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Column(name="dorm_id")
	public int getDormId() {
		return this.dormId;
	}

	public void setDormId(int dormId) {
		this.dormId = dormId;
	}


	@Column(name="member_id")
	public int getMemberId() {
		return this.memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

}