package com.spring.dormnbu.cores.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the member database table.
 * 
 */
@Entity
@Table(name="member")
public class MemberEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer memberId;
	private String firstName;
	private String lastName;
	private String phone;
	private String email;
	private String password;
	private Date createDate;
	private Date modify;
	private String picture;
	private String certficate;
	private Character memberType;
	private Character status;
	
	public MemberEntity() {
		
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="member_id")
	public Integer getMemberId() {
		return this.memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}


	public String getCertficate() {
		return certficate;
	}
	public void setCertficate(String certficate) {
		this.certficate = certficate;
	}


	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="create_date")
	public Date getCreateDate() {
		return this.createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	@Column(name="first_name")
	public String getFirstName() {
		return this.firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	@Column(name="last_name")
	public String getLastName() {
		return this.lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	@Column(name="member_type")
	public Character getMemberType() {
		return this.memberType;
	}
	public void setMemberType(Character memberType) {
		this.memberType = memberType;
	}


	@Temporal(TemporalType.TIMESTAMP)
	public Date getModify() {
		return this.modify;
	}
	public void setModify(Date modify) {
		this.modify = modify;
	}


	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


	public String getPhone() {
		return this.phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getPicture() {
		return this.picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}


	public Character getStatus() {
		return this.status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	
}