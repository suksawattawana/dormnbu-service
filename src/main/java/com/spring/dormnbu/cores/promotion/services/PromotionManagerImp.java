package com.spring.dormnbu.cores.promotion.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionManager;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionService;

@Component
@Transactional
@Scope("request")
public class PromotionManagerImp implements PromotionManager{
	
	@Autowired
	private PromotionService service;

	@Override
	public Promotion add(Promotion value) throws Exception {
		Promotion result = null;
		
		//1. ตรวจสอบ criteria
		if (value != null) {
			
			//2. ตรวจสอบข้อมูลซ้ำ
			result = service.add(value);
		}
		
		return result;
	}
	
	@Override
	public Promotion edit(Promotion value) throws Exception {
		Promotion result = null;
		
		//1. ตรวจสอบ criteria
		if (value != null && value.getPromotionId() != 0) {
			
			//2. ค้นหาข้อมูล
			result = service.edit(value);
		}
		
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ตรวจสอบว่ามีข้อมูลหรือไม่
				Promotion data = service.findById(id);
				
				if(data != null){
					//2. ลบข้อมูล
					service.delete(id);
				}
				else{
					throw new DataNotFoundException("No Data for key: " + id);
				}
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Promotion findById(int id) throws Exception {
		Promotion result = null;
		
		//1. ตรวจสอบ criteria
		if (id != 0) {
			
			//2. ค้นหาข้อมูล
			result = service.findById(id);
		}
		
		return result;
	}

}


