package com.spring.dormnbu.cores.promotion.interfaces;

import com.spring.dormnbu.cores.entity.PromotionEntity;

public interface PromotionDao {

	
	PromotionEntity add(PromotionEntity value) throws Exception;
	PromotionEntity findById(int id) throws Exception;
	PromotionEntity edit(PromotionEntity value) throws Exception;
	void delete(int id) throws Exception;
}
