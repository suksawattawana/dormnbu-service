package com.spring.dormnbu.cores.promotion.interfaces;

import com.spring.dormnbu.cores.dorm.domain.Promotion;

public interface PromotionService {


	Promotion add(Promotion value) throws Exception;
	Promotion findById(int id) throws Exception;
	Promotion edit(Promotion value) throws Exception;
	void delete(int id) throws Exception;
}
