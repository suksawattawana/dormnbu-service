package com.spring.dormnbu.cores.promotion.services;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.entity.PromotionEntity;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionDao;

@Repository
@Transactional
@Scope("request")
public class PromotionDaoImp implements PromotionDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public PromotionEntity add(PromotionEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public PromotionEntity edit(PromotionEntity value) throws Exception {
		try{
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public void delete(int id) throws Exception {
		try {
			//1. map sql 
			Query query = em.createNamedQuery("Promotion.delete");
			query.setParameter("promotionId", id);
			
			//2. query
			query.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public PromotionEntity findById(int id) throws Exception {
		PromotionEntity result = null;
		
		try{
			result = em.find(PromotionEntity.class, id);
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	
}
