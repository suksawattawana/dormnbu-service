package com.spring.dormnbu.cores.promotion.services;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.entity.PromotionEntity;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionDao;
import com.spring.dormnbu.cores.promotion.interfaces.PromotionService;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class PromotionServiceImp implements PromotionService {
	
	@Autowired
	private PromotionDao dao;

	@Override
	public Promotion add(Promotion value) throws Exception {
		Promotion result = null;
		
		try {
			//2 จัดกาข้อมูล
			PromotionEntity data = new PromotionEntity();
			data.setStartDate(DateTimeUtil.stringToDate(value.getStartDate()));
			data.setEndDate(DateTimeUtil.stringToDate(value.getEndDate()));
			data.setDetail(value.getDetail());
			
			//3. อัพเดทข้อมูล
			dao.add(data);
			
			//4. return ข้อมุล
			result = new Promotion();
			result.setStartDate(DateTimeUtil.dateToString(data.getStartDate()));
			result.setEndDate(DateTimeUtil.dateToString(data.getEndDate()));
			result.setDetail(value.getDetail());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public Promotion edit(Promotion value) throws Exception {
		Promotion result = null;
		
		try {
			//1. ค้นหาข้อมูล
			PromotionEntity data = dao.findById(value.getPromotionId());
			
			//2. จัดการผลลัพธ์
			if (data != null){
				
				//2.1 ข้อมูลที่มีการแก้ไข
				data.setDetail(value.getDetail());
				data.setStartDate(DateTimeUtil.stringToDate(value.getStartDate()));
				data.setEndDate(DateTimeUtil.stringToDate(value.getEndDate()));
				
				//3. อัพเดทข้อมูล
				dao.edit(data);
				
				//4. return ข้อมุล
				result = new Promotion();
				BeanUtils.copyProperties(result, data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Promotion findById(int id) throws Exception {
		Promotion result = null;
		
		try {
			//1. ค้นหาข้อมูล
			PromotionEntity data = dao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = new Promotion();
				
				//2.1 copy ข้อมูลหลัก
				BeanUtils.copyProperties(result, data);
						
				//2.2 ข้อมูลเพิ่ม
				result.setStartDate(DateTimeUtil.dateToString(data.getStartDate()));
				result.setEndDate(DateTimeUtil.dateToString(data.getEndDate()));
				result.setDetail(data.getDetail());
			}
				
		} catch (Exception e) {
				throw e;
		}
			
		return result;
	}
	
	@Override
	public void delete(int id) throws Exception {
		dao.delete(id);	
	}
}