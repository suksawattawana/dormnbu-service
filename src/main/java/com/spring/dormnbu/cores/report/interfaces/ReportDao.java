package com.spring.dormnbu.cores.report.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.dorm.domain.Dorm;

public interface ReportDao {

	List<Dorm> findDormByCriteria(DormCriteria criteria) throws Exception;
}
