package com.spring.dormnbu.cores.report.interfaces;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.report.domain.ExportPDF;

public interface ReportManager {

	XSSFWorkbook exportListDormExel(DormCriteria criteria) throws Exception;
	
	ExportPDF exportDormById(int id) throws Exception;
}
