package com.spring.dormnbu.cores.report.domain;

import java.io.Serializable;

public class ExportPDF implements Serializable {

	private static final long serialVersionUID = 1L;
	private byte[] pdfFile;
	private String pdfName;
	
	public byte[] getPdfFile() {
		return pdfFile;
	}
	public void setPdfFile(byte[] pdfFile) {
		this.pdfFile = pdfFile;
	}
	public String getPdfName() {
		return pdfName;
	}
	public void setPdfName(String pdfName) {
		this.pdfName = pdfName;
	}
	
}
