package com.spring.dormnbu.cores.report.services;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.PageOrder;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.entity.AmenitieEntity;
import com.spring.dormnbu.cores.entity.DormEntity;
import com.spring.dormnbu.cores.entity.RoomtypeEntity;
import com.spring.dormnbu.cores.report.domain.DormById;
import com.spring.dormnbu.cores.report.domain.ExportPDF;
import com.spring.dormnbu.cores.report.interfaces.ReportDao;
import com.spring.dormnbu.cores.report.interfaces.ReportService;
import com.spring.dormnbu.enums.CostUtilities;
import com.spring.dormnbu.utils.DateTimeUtil;
import com.spring.dormnbu.utils.ReportPOIUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;

@Service
@Transactional
@Scope("request")
public class ReportServiceImp implements ReportService {
	
	@Autowired
	private ReportDao dao;
	
	@Autowired
	private DormDao dormDao;
	
	XSSFWorkbook wb = new XSSFWorkbook();
	
	// Create Cell Style
	XSSFCellStyle styleHeader;
	XSSFCellStyle styleCriteriaLabel;
	XSSFCellStyle styleCriteriaValue;
	XSSFCellStyle styleHeaderTable;
	XSSFCellStyle styleValue;
	XSSFCellStyle styleValueCenter;
	
	public ReportServiceImp() {
		try {
			styleHeader = ReportPOIUtil.createHeaderStyle(wb);
			styleCriteriaLabel = ReportPOIUtil.createCriteriaLabelStyle(wb);
			styleCriteriaValue = ReportPOIUtil.createCriteriaValueStyle(wb);
		} catch (Exception e) {
			
		}
	}

	@Override
	public XSSFWorkbook exportListDormExel(List<Dorm> result, DormCriteria criteria) throws Exception {
		
		try {
			//1.1 สร้างเอกสาร
			XSSFSheet sheet = wb.createSheet("หอพัก");
			int rowIndex = 0;
			
			//1.2 ตั้งค่าคุณสมบัติเอกสาร
			sheet.setAutobreaks(true);
			sheet.setFitToPage(true);
			sheet.getPrintSetup().setValidSettings(true);
			sheet.getPrintSetup().setLandscape(true);		// เอกสารแนวนอน
			sheet.getPrintSetup().setFitHeight((short) 0);	// fit กระดาษตั้ง
			sheet.getPrintSetup().setFitWidth((short) 1);	// fit กระดาษแนวนอน
			sheet.getPrintSetup().setPaperSize(PrintSetup.A4_PAPERSIZE);
			sheet.getPrintSetup().setPageOrder(PageOrder.OVER_THEN_DOWN);
			
			//2.1 Header Criteria
			rowIndex = createHeaderCriteriaForPageSeaech(sheet, rowIndex, criteria);
			
			//2.2 Body
		} catch (Exception e) {
			throw e;
		}
	      
		return wb;
	}
	
	private int createHeaderCriteriaForPageSeaech(XSSFSheet sheet, int rIndex, DormCriteria criteria) throws Exception {
		
		int rowIndex = rIndex;
		int colIndex = 0;
		int colEnd = 8;
		String seperlate = " : ";
		
		try {
			// Row : Report Name
			Row row = sheet.createRow(rowIndex);
			Cell cell = row.createCell(colIndex);
			cell.setCellStyle(styleHeader);
			sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, colEnd));
			cell.setCellValue("รายชื่อหอพักที่ลงทะเบียน");
			
			// Row : dormCode, name
			rowIndex++;
			row = sheet.createRow(rowIndex);
			
			colIndex = 0;
			cell = row.createCell(colIndex);
			sheet.addMergedRegion(new CellRangeAddress(rowIndex, rowIndex, colIndex, ++colIndex));
			cell.setCellStyle(styleCriteriaLabel);
			cell.setCellValue("รหัสหอพัก" + seperlate);
			
		} catch (Exception e) {
			throw e;
		}
		
		
		return rowIndex;
	}

	@Override
	public ExportPDF exportDormById(DormById value) throws Exception {
		ExportPDF result = null;
		
		try {
			//1. get file path
			String rpFile = getFilePath("/report/dorm_id.jasper");
			
			//2.1 create parameter
			HashMap<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("printDate", DateTimeUtil.getStringCurentDate());
			parameters.put("img", getFilePath("/report/img/logo_dormnbu.png"));
			
			//2.2 create data
			ArrayList<DormById> listData = new ArrayList<DormById>();
			listData.add(value);
			JRDataSource dataSource = new JRBeanCollectionDataSource(listData);
			
			if (rpFile != null) {
				
				JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(rpFile);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
				
				byte[] pdfFile = JasperExportManager.exportReportToPdf(jasperPrint);
				String pdfName = "dormnbu - " + value.getNameTh();
				
				result = new ExportPDF();
				result.setPdfFile(pdfFile);
				result.setPdfName(pdfName);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return result;
	}

	@Override
	public DormById findDormById(int id) throws Exception {
		DormById result = null;
		
		try {
			// ค้นหาข้อมูล
			DormEntity data = dormDao.findById(id);
			
			// จัดการผลลัพธ์
			if (data != null){
				
				//1.1 set main value
				result = new DormById();
				result.setDormCode(data.getDormCode());
				result.setNameTh(data.getNameTh());
				result.setNameEn(data.getNameEn());
				result.setPhone1(data.getPhone1());
				result.setPhone2(data.getPhone2());
				result.setEmail(data.getEmail());
				result.setLine(data.getLine());
				result.setDetail(data.getDetail());

				// ค่าน้ำ-ค่าไฟ
				manageWater(result, data);
				manageElectricity(result, data);
				
				// ค่าบริการอื่นๆ
				manageOrtherService(result, data);
				
				// ค่าอินเทอร์เน็ต
				manageInternet(result, data);
				
				// เงินมัดจำ
				manageDeposit(result, data);
				
				// จ่ายล่วงหน้า
//				result.setPayadvanceType(data.getPayadvanceType());
//				result.setPayadvancePrice(data.getPayadvancePrice());
				
				//1.2 set area value
				result.setAreaId(data.getArea().getAreaId());
				result.setNo(data.getArea().getNo());
				result.setRoad(data.getArea().getRoad());
				result.setSoi(data.getArea().getSoi());
				
				//1.3 set amenitie value
				AmenitieEntity amen = data.getAmenitie();
				result.setAir(manageAmenitie(amen.getAir()));
				result.setCable(manageAmenitie(amen.getCable()));
				result.setCctv(manageAmenitie(amen.getCctv()));
				result.setFan(manageAmenitie(amen.getFan()));
				result.setFurniture(manageAmenitie(amen.getFurniture()));
				result.setGymnasium(manageAmenitie(amen.getGymnasium()));
				result.setHairSalon(manageAmenitie(amen.getHairSalon()));
				result.setKeycard(manageAmenitie(amen.getKeycard()));
				result.setLift(manageAmenitie(amen.getLift()));
				result.setWashing(manageAmenitie(amen.getWashing()));
				result.setWaterHeater(manageAmenitie(amen.getWaterHeater()));
				result.setWifi(manageAmenitie(amen.getWifi()));
				
				//1.4 set roomType value
				List<Roomtype> roomtypes = new ArrayList<>();
				for (RoomtypeEntity roomtypeEntity : data.getRoomtypes()) {
					Roomtype roomtype = new Roomtype();
					BeanUtils.copyProperties(roomtype, roomtypeEntity);
					roomtypes.add(roomtype);
				}
				result.setRoomtypes(roomtypes);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	private String manageAmenitie(Character value){
		String result = null;
		
		if (value.equals('Y')) {
			result = "มี";
		}else {
			result = "-";
		}
		
		return result;
	}
	
	// จัดการประเภทค่าไฟ
	private void manageElectricity(DormById result, DormEntity data){
		if (data.getElectricityType().toString().equals(CostUtilities.PACKAGES.getKey())) {
			result.setElectricityDesc( 
				CostUtilities.findValue(data.getElectricityType().toString())
				+ " ( "
				+ data.getElectricityPrice()
				+ " บาท/เดือน"
				+ ", สูงสุด "
				+ data.getElectricityMax()
				+ " ยูนิต )"
			);
		}
		else if (data.getElectricityType().toString().equals(CostUtilities.UNIT_USE.getKey())) {
			result.setElectricityDesc( 
				CostUtilities.findValue(data.getElectricityType().toString())
				+ " ( "
				+ data.getElectricityPrice()
				+ " บาท/ยูนิต"
				+ " )"
			);
		}
		else {
			result.setElectricityDesc(CostUtilities.findValue(data.getElectricityType().toString()));
		}
	}
	
	// จัดการประเภทค่าน้ำ
	private void manageWater(DormById result, DormEntity data){
		if (data.getWaterType().toString().equals(CostUtilities.PACKAGES.getKey())) {
			result.setWaterDesc( 
				CostUtilities.findValue(data.getWaterType().toString())
				+ " ( "
				+ data.getWaterPrice()
				+ " บาท/เดือน"
				+ ", สูงสุด "
				+ data.getWaterMax()
				+ " ยูนิต )"
			);
		}
		else if (data.getWaterType().toString().equals(CostUtilities.UNIT_USE.getKey())) {
			result.setWaterDesc( 
				CostUtilities.findValue(data.getWaterType().toString())
				+ " ( "
				+ data.getWaterPrice()
				+ " บาท/ยูนิต"
				+ " )"
			);
		}
		else {
			result.setWaterDesc(CostUtilities.findValue(data.getWaterType().toString()));
		}
	}
	
	private void manageOrtherService(DormById result, DormEntity data) {
		if (data.getOtherServiceType().equals('Y')) {
			result.setOtherServiceDesc(
				data.getOtherServiceDesc()
				+ " ( "
				+ data.getOtherServicePrice()
				+ " บาท"
				+ " )"
			);
		}
		else {
			result.setOtherServiceDesc("-");
		}
	}
	
	private void manageInternet(DormById result, DormEntity data) {
		if (data.getInternetType().toString().equals(CostUtilities.INTERNET_HAVE.getKey())) {
			result.setInternetDesc(
				data.getInternetPrice()
				+ " บาท/เดือน"
			);
		}
		else if (data.getInternetType().toString().equals(CostUtilities.INTERNET_NOT.getKey())) {
			result.setInternetDesc(CostUtilities.findValue(data.getInternetType().toString()));
		}
		else if (data.getInternetType().toString().equals(CostUtilities.INTERNET_FREE.getKey())) {
			result.setInternetDesc(CostUtilities.findValue(data.getInternetType().toString()));
		}
		else if (data.getInternetType().toString().equals(CostUtilities.INTERNET_CONTACT.getKey())) {
			result.setInternetDesc(CostUtilities.findValue(data.getInternetType().toString()));
		}
		else {
			result.setInternetDesc("-");
		}
	}

	private void manageDeposit(DormById result, DormEntity data) {
		if (data.getDepositType().toString().equals(CostUtilities.DEPOSIT_HAVE.getKey())) {
			result.setDepositDesc(
				data.getDepositPrice()
				+ " บาท"
			);
		}
		else if (data.getDepositType().toString().equals(CostUtilities.DEPOSIT_NOT.getKey())) {
			result.setDepositDesc(CostUtilities.findValue(data.getDepositType().toString()));
		}
		else {
			result.setDepositDesc("-");
		}
	}
	
	private String getFilePath(String path) {
		URL resource = getClass().getResource(path);
		String imgFile = null;
		
		try {
			File file = Paths.get(resource.toURI()).toFile();
			imgFile = file.getAbsolutePath();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		return imgFile;
	}

	@Override
	public List<Dorm> findDormByCriteria(DormCriteria criteria) throws Exception {
		List<Dorm> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<Dorm> listData = dao.findDormByCriteria(criteria);
			System.out.println("listData-size: " + listData.size());
			
			if (listData != null && !listData.isEmpty()) {
				
				result = new ArrayList<>();
				for (Dorm dorm : listData) {
					Dorm data = new Dorm();
					data.setDormId(dorm.getDormId());
					result.add(data);
				}
				
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
