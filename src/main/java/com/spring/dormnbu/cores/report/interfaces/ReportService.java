package com.spring.dormnbu.cores.report.interfaces;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.report.domain.DormById;
import com.spring.dormnbu.cores.report.domain.ExportPDF;

public interface ReportService {

	// Export
	XSSFWorkbook exportListDormExel(List<Dorm> result, DormCriteria criteria) throws Exception;
	
	ExportPDF exportDormById(DormById value) throws Exception;
	
	// Query
	DormById findDormById(int id) throws Exception;
	
	List<Dorm> findDormByCriteria(DormCriteria criteria) throws Exception;
}
