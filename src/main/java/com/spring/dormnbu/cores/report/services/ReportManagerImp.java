package com.spring.dormnbu.cores.report.services;

import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.report.domain.DormById;
import com.spring.dormnbu.cores.report.domain.ExportPDF;
import com.spring.dormnbu.cores.report.interfaces.ReportManager;
import com.spring.dormnbu.cores.report.interfaces.ReportService;

@Component
@Transactional
@Scope("request")
public class ReportManagerImp implements ReportManager {

	@Autowired
	private ReportService service;
	
	@Override
	public XSSFWorkbook exportListDormExel(DormCriteria criteria) throws Exception {
		XSSFWorkbook result = null;
		
		try {
			//1. ค้นหาข้อมูล
			List<Dorm> listData = service.findDormByCriteria(criteria);
			
			if (listData != null && !listData.isEmpty()) {
				result = service.exportListDormExel(listData, criteria);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public ExportPDF exportDormById(int id) throws Exception {
		ExportPDF result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				DormById dorm = service.findDormById(id);
				
				//3. ออกรายงาน
				if (dorm != null) {
					result = service.exportDormById(dorm);
				}
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
}
