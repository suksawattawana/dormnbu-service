package com.spring.dormnbu.cores.report.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.sql.IgnoreCaseAliasToBeanResultTransformer;
import com.spring.dormnbu.commons.sql.SQLUtils;
import com.spring.dormnbu.cores.domain.DormCriteria;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.report.interfaces.ReportDao;

@Repository
@Transactional
@Scope("request")
public class ReportDaoImp implements ReportDao {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public List<Dorm> findDormByCriteria(DormCriteria criteria) throws Exception {
		List<Dorm> result = null;
		
		try{
			//1. map sql 
			Query query = SQLUtils.getQuery(em, "Report.findDormByCriteria", criteria);
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(Dorm.class));
			
			//2. query
			result = query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
}
