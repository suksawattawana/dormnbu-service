package com.spring.dormnbu.cores.report.domain;

import java.io.Serializable;
import java.util.List;

import com.spring.dormnbu.cores.dorm.domain.Roomtype;

public class DormById implements Serializable {

	private static final long serialVersionUID = 1L;
	private String dormCode;
	
	private String nameTh;
	private String nameEn;
	private String phone1;
	private String phone2;
	private String email;
	private String line;
	private String detail;
	
	private String waterDesc;
	private String electricityDesc;
	private String otherServiceDesc;
	private String internetDesc;
	private String depositDesc;
	private String payadvanceDesc;
	
	// amenitie
	private String air;
	private String cable;
	private String cctv;
	private String fan;
	private String furniture;
	private String gymnasium;
	private String hairSalon;
	private String keycard;
	private String lift;
	private String washing;
	private String waterHeater;
	private String wifi;
	
	// area
	private int areaId;
	private String no;
	private String road;
	private String soi;
	
	private List<Roomtype> roomtypes;


	public String getDormCode() {
		return dormCode;
	}

	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}

	public String getNameTh() {
		return nameTh;
	}

	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}

	public String getNameEn() {
		return nameEn;
	}

	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getOtherServiceDesc() {
		return otherServiceDesc;
	}

	public void setOtherServiceDesc(String otherServiceDesc) {
		this.otherServiceDesc = otherServiceDesc;
	}

	public String getInternetDesc() {
		return internetDesc;
	}

	public void setInternetDesc(String internetDesc) {
		this.internetDesc = internetDesc;
	}

	public String getDepositDesc() {
		return depositDesc;
	}

	public void setDepositDesc(String depositDesc) {
		this.depositDesc = depositDesc;
	}

	public String getPayadvanceDesc() {
		return payadvanceDesc;
	}

	public void setPayadvanceDesc(String payadvanceDesc) {
		this.payadvanceDesc = payadvanceDesc;
	}

	public String getAir() {
		return air;
	}

	public void setAir(String air) {
		this.air = air;
	}

	public String getCable() {
		return cable;
	}

	public void setCable(String cable) {
		this.cable = cable;
	}

	public String getCctv() {
		return cctv;
	}

	public void setCctv(String cctv) {
		this.cctv = cctv;
	}

	public String getFan() {
		return fan;
	}

	public void setFan(String fan) {
		this.fan = fan;
	}

	public String getFurniture() {
		return furniture;
	}

	public void setFurniture(String furniture) {
		this.furniture = furniture;
	}

	public String getGymnasium() {
		return gymnasium;
	}

	public void setGymnasium(String gymnasium) {
		this.gymnasium = gymnasium;
	}

	public String getHairSalon() {
		return hairSalon;
	}

	public void setHairSalon(String hairSalon) {
		this.hairSalon = hairSalon;
	}

	public String getKeycard() {
		return keycard;
	}

	public void setKeycard(String keycard) {
		this.keycard = keycard;
	}

	public String getLift() {
		return lift;
	}

	public void setLift(String lift) {
		this.lift = lift;
	}

	public String getWashing() {
		return washing;
	}

	public void setWashing(String washing) {
		this.washing = washing;
	}

	public String getWaterHeater() {
		return waterHeater;
	}

	public void setWaterHeater(String waterHeater) {
		this.waterHeater = waterHeater;
	}

	public String getWifi() {
		return wifi;
	}

	public void setWifi(String wifi) {
		this.wifi = wifi;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getSoi() {
		return soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	public List<Roomtype> getRoomtypes() {
		return roomtypes;
	}

	public void setRoomtypes(List<Roomtype> roomtypes) {
		this.roomtypes = roomtypes;
	}

	public String getWaterDesc() {
		return waterDesc;
	}

	public void setWaterDesc(String waterDesc) {
		this.waterDesc = waterDesc;
	}

	public String getElectricityDesc() {
		return electricityDesc;
	}

	public void setElectricityDesc(String electricityDesc) {
		this.electricityDesc = electricityDesc;
	}
	

}
