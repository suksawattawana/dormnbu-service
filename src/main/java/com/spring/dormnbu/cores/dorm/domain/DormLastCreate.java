package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class DormLastCreate implements Serializable{

	private static final long serialVersionUID = -5663955074014291067L;
	private int dormId;
	private String dormCode;
	
	private String name;
	private String address;
	private String picture;
	private String createDate;
	
	private Character monthlyRent;
	private int monthlyMax;
	private int monthlyMin;
	
	private Character dailyRent;
	private int dailyMax;
	private int dailyMin;
	
	public int getDormId() {
		return dormId;
	}
	public void setDormId(int dormId) {
		this.dormId = dormId;
	}
	public String getDormCode() {
		return dormCode;
	}
	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Character getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(Character monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	public int getMonthlyMax() {
		return monthlyMax;
	}
	public void setMonthlyMax(int monthlyMax) {
		this.monthlyMax = monthlyMax;
	}
	public int getMonthlyMin() {
		return monthlyMin;
	}
	public void setMonthlyMin(int monthlyMin) {
		this.monthlyMin = monthlyMin;
	}
	public Character getDailyRent() {
		return dailyRent;
	}
	public void setDailyRent(Character dailyRent) {
		this.dailyRent = dailyRent;
	}
	public int getDailyMax() {
		return dailyMax;
	}
	public void setDailyMax(int dailyMax) {
		this.dailyMax = dailyMax;
	}
	public int getDailyMin() {
		return dailyMin;
	}
	public void setDailyMin(int dailyMin) {
		this.dailyMin = dailyMin;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	
}
