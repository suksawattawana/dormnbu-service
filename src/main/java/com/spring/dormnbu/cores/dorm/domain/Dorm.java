package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;
import java.util.List;

public class Dorm implements Serializable{
	
	private static final long serialVersionUID = 8731965245378280076L;
	private int dormId;
	private String dormCode;
	private int memberId;
	
	private int areaId;
	private int promotionId;
	private int pictureId;
	private int amenitieId;
	
	private String nameTh;
	private String nameEn;
	private String phone1;
	private String phone2;
	private String email;
	private String line;
	private String detail;
	
	private Character waterType;
	private int waterMax;
	private int waterMin;
	private int waterPrice;
	
	private Character electricityType;
	private int electricityMax;
	private int electricityMin;
	private int electricityPrice;
	
	private Character otherServiceType;
	private String otherServiceDesc;
	private int otherServicePrice;
	
	private Character internetType;
	private int internetPrice;
	private Character depositType;
	private int depositPrice;
	private Character payadvanceType;
	private int payadvancePrice;
	
	private String createDate;
	private String modify;
	
	private Amenitie amenitie;
	private Area area;
	private Picture picture;
	private Promotion promotion;
	private List<Roomtype> roomtypes;
	
	
	public int getDormId() {
		return dormId;
	}
	public void setDormId(int dormId) {
		this.dormId = dormId;
	}
	public String getDormCode() {
		return dormCode;
	}
	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
	public String getNameTh() {
		return nameTh;
	}
	public void setNameTh(String nameTh) {
		this.nameTh = nameTh;
	}
	public String getNameEn() {
		return nameEn;
	}
	public void setNameEn(String nameEn) {
		this.nameEn = nameEn;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Character getWaterType() {
		return waterType;
	}
	public void setWaterType(Character waterType) {
		this.waterType = waterType;
	}
	public int getWaterMax() {
		return waterMax;
	}
	public void setWaterMax(int waterMax) {
		this.waterMax = waterMax;
	}
	public int getWaterMin() {
		return waterMin;
	}
	public void setWaterMin(int waterMin) {
		this.waterMin = waterMin;
	}
	public int getWaterPrice() {
		return waterPrice;
	}
	public void setWaterPrice(int waterPrice) {
		this.waterPrice = waterPrice;
	}
	public Character getElectricityType() {
		return electricityType;
	}
	public void setElectricityType(Character electricityType) {
		this.electricityType = electricityType;
	}
	public int getElectricityMax() {
		return electricityMax;
	}
	public void setElectricityMax(int electricityMax) {
		this.electricityMax = electricityMax;
	}
	public int getElectricityMin() {
		return electricityMin;
	}
	public void setElectricityMin(int electricityMin) {
		this.electricityMin = electricityMin;
	}
	public int getElectricityPrice() {
		return electricityPrice;
	}
	public void setElectricityPrice(int electricityPrice) {
		this.electricityPrice = electricityPrice;
	}
	public Character getOtherServiceType() {
		return otherServiceType;
	}
	public void setOtherServiceType(Character otherServiceType) {
		this.otherServiceType = otherServiceType;
	}
	public String getOtherServiceDesc() {
		return otherServiceDesc;
	}
	public void setOtherServiceDesc(String otherServiceDesc) {
		this.otherServiceDesc = otherServiceDesc;
	}
	public int getOtherServicePrice() {
		return otherServicePrice;
	}
	public void setOtherServicePrice(int otherServicePrice) {
		this.otherServicePrice = otherServicePrice;
	}
	public Character getInternetType() {
		return internetType;
	}
	public void setInternetType(Character internetType) {
		this.internetType = internetType;
	}
	public int getInternetPrice() {
		return internetPrice;
	}
	public void setInternetPrice(int internetPrice) {
		this.internetPrice = internetPrice;
	}
	public Character getDepositType() {
		return depositType;
	}
	public void setDepositType(Character depositType) {
		this.depositType = depositType;
	}
	public int getDepositPrice() {
		return depositPrice;
	}
	public void setDepositPrice(int depositPrice) {
		this.depositPrice = depositPrice;
	}
	public Character getPayadvanceType() {
		return payadvanceType;
	}
	public void setPayadvanceType(Character payadvanceType) {
		this.payadvanceType = payadvanceType;
	}
	public int getPayadvancePrice() {
		return payadvancePrice;
	}
	public void setPayadvancePrice(int payadvancePrice) {
		this.payadvancePrice = payadvancePrice;
	}
	public Amenitie getAmenitie() {
		return amenitie;
	}
	public void setAmenitie(Amenitie amenitie) {
		this.amenitie = amenitie;
	}
	public Area getArea() {
		return area;
	}
	public void setArea(Area area) {
		this.area = area;
	}
	public Picture getPicture() {
		return picture;
	}
	public void setPicture(Picture picture) {
		this.picture = picture;
	}
	public Promotion getPromotion() {
		return promotion;
	}
	public void setPromotion(Promotion promotion) {
		this.promotion = promotion;
	}
	public List<Roomtype> getRoomtypes() {
		return roomtypes;
	}
	public void setRoomtypes(List<Roomtype> roomtypes) {
		this.roomtypes = roomtypes;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getModify() {
		return modify;
	}
	public void setModify(String modify) {
		this.modify = modify;
	}
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	public int getPictureId() {
		return pictureId;
	}
	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}
	public int getAmenitieId() {
		return amenitieId;
	}
	public void setAmenitieId(int amenitieId) {
		this.amenitieId = amenitieId;
	}
	
	
}
