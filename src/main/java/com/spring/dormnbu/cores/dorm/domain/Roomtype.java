package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class Roomtype implements Serializable{
	
	private static final long serialVersionUID = -7834590496357330647L;
	private int roomtypeId;
	private int dormId;
	private String type;
	private String format;
	private String size;
	private String unit;
	private Character status;
	
	private Character monthlyRent;
	private int monthlyMax;
	private int monthlyMin;
	
	private Character dailyRent;
	private int dailyMax;
	private int dailyMin;
	
	
	public int getRoomtypeId() {
		return roomtypeId;
	}
	public void setRoomtypeId(int roomtypeId) {
		this.roomtypeId = roomtypeId;
	}
	public int getDormId() {
		return dormId;
	}
	public void setDormId(int dormId) {
		this.dormId = dormId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	public Character getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(Character monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	public int getMonthlyMax() {
		return monthlyMax;
	}
	public void setMonthlyMax(int monthlyMax) {
		this.monthlyMax = monthlyMax;
	}
	public int getMonthlyMin() {
		return monthlyMin;
	}
	public void setMonthlyMin(int monthlyMin) {
		this.monthlyMin = monthlyMin;
	}
	public Character getDailyRent() {
		return dailyRent;
	}
	public void setDailyRent(Character dailyRent) {
		this.dailyRent = dailyRent;
	}
	public int getDailyMax() {
		return dailyMax;
	}
	public void setDailyMax(int dailyMax) {
		this.dailyMax = dailyMax;
	}
	public int getDailyMin() {
		return dailyMin;
	}
	public void setDailyMin(int dailyMin) {
		this.dailyMin = dailyMin;
	}
	
	
}
