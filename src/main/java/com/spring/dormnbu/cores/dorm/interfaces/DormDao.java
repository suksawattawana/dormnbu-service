package com.spring.dormnbu.cores.dorm.interfaces;

import java.util.Date;
import java.util.List;

import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.entity.AmenitieEntity;
import com.spring.dormnbu.cores.entity.AreaEntity;
import com.spring.dormnbu.cores.entity.DormEntity;
import com.spring.dormnbu.cores.entity.PictureEntity;
import com.spring.dormnbu.cores.entity.PromotionEntity;
import com.spring.dormnbu.cores.entity.RoomtypeEntity;

public interface DormDao {
	
	// CheckDup
	DormEntity checkDup(Dorm value) throws Exception;
	
	String checkDormCode(String value) throws Exception;
	
	Integer checkNullDorm(int id) throws Exception;

	// Add
	DormEntity add(DormEntity value) throws Exception;
	
	RoomtypeEntity addRoomtype(RoomtypeEntity value) throws Exception;
	
	// Edit
	DormEntity edit(DormEntity value) throws Exception;
	
	AmenitieEntity editAmenitie(AmenitieEntity value) throws Exception;
	
	AreaEntity editArea(AreaEntity value) throws Exception;
	
	PictureEntity editPicture(PictureEntity value) throws Exception;
	
	PromotionEntity editPromotion(PromotionEntity value) throws Exception;
	
	RoomtypeEntity editRoomtype(RoomtypeEntity value) throws Exception;
	
	// Delete
	void delete(int dormId) throws Exception;
	
	void deleteRoomtype(RoomtypeEntity value) throws Exception;
	
	void deleteByDormEntity(DormEntity value) throws Exception;
	
	// Query
	DormEntity findById(int dormId) throws Exception;
	
	AmenitieEntity findAmenitieById(int id) throws Exception;
	
	AreaEntity findAreaById(int id) throws Exception;
	
	PictureEntity findPictureById(int id) throws Exception;
	
	PromotionEntity findPromotionById(int id) throws Exception;
	
	RoomtypeEntity findRoomtypeById(int id) throws Exception;
	
	List<Roomtype> findRoomtypeBydormId(int dormId) throws Exception;
	
	// Query - Where
	List<DormLastCreate> queryLastCreate(Date currentMonth) throws Exception;
	
	List<DormPopular> queryPopular() throws Exception;
	
}
