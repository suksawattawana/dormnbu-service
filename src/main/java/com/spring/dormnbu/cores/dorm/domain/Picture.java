package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class Picture implements Serializable {
	
	private static final long serialVersionUID = 8323502022678120566L;
	private int pictureId;
	private String picture1;
	private String picture2;
	private String picture3;
	private String picture4;
	private String picture5;
	private String picture6;
	private String picture7;
	private String picture8;
	private String picture9;
	private String picture10;
	
	public int getPictureId() {
		return pictureId;
	}
	public void setPictureId(int pictureId) {
		this.pictureId = pictureId;
	}
	public String getPicture1() {
		return picture1;
	}
	public void setPicture1(String picture1) {
		this.picture1 = picture1;
	}
	public String getPicture2() {
		return picture2;
	}
	public void setPicture2(String picture2) {
		this.picture2 = picture2;
	}
	public String getPicture3() {
		return picture3;
	}
	public void setPicture3(String picture3) {
		this.picture3 = picture3;
	}
	public String getPicture4() {
		return picture4;
	}
	public void setPicture4(String picture4) {
		this.picture4 = picture4;
	}
	public String getPicture5() {
		return picture5;
	}
	public void setPicture5(String picture5) {
		this.picture5 = picture5;
	}
	public String getPicture6() {
		return picture6;
	}
	public void setPicture6(String picture6) {
		this.picture6 = picture6;
	}
	public String getPicture7() {
		return picture7;
	}
	public void setPicture7(String picture7) {
		this.picture7 = picture7;
	}
	public String getPicture8() {
		return picture8;
	}
	public void setPicture8(String picture8) {
		this.picture8 = picture8;
	}
	public String getPicture9() {
		return picture9;
	}
	public void setPicture9(String picture9) {
		this.picture9 = picture9;
	}
	public String getPicture10() {
		return picture10;
	}
	public void setPicture10(String picture10) {
		this.picture10 = picture10;
	}
	
}
