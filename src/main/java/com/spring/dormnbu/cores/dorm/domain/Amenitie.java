package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class Amenitie implements Serializable {
	
	private static final long serialVersionUID = 760122697356807254L;
	private int amenitieId;
	private Character air;
	private Character cable;
	private Character cctv;
	private Character fan;
	private Character furniture;
	private Character gymnasium;
	private Character hairSalon;
	private Character keycard;
	private Character lift;
	private Character washing;
	private Character waterHeater;
	private Character wifi;
	
	public int getAmenitieId() {
		return amenitieId;
	}
	public void setAmenitieId(int amenitieId) {
		this.amenitieId = amenitieId;
	}
	public Character getAir() {
		return air;
	}
	public void setAir(Character air) {
		this.air = air;
	}
	public Character getCable() {
		return cable;
	}
	public void setCable(Character cable) {
		this.cable = cable;
	}
	public Character getCctv() {
		return cctv;
	}
	public void setCctv(Character cctv) {
		this.cctv = cctv;
	}
	public Character getFan() {
		return fan;
	}
	public void setFan(Character fan) {
		this.fan = fan;
	}
	public Character getFurniture() {
		return furniture;
	}
	public void setFurniture(Character furniture) {
		this.furniture = furniture;
	}
	public Character getGymnasium() {
		return gymnasium;
	}
	public void setGymnasium(Character gymnasium) {
		this.gymnasium = gymnasium;
	}
	public Character getHairSalon() {
		return hairSalon;
	}
	public void setHairSalon(Character hairSalon) {
		this.hairSalon = hairSalon;
	}
	public Character getKeycard() {
		return keycard;
	}
	public void setKeycard(Character keycard) {
		this.keycard = keycard;
	}
	public Character getLift() {
		return lift;
	}
	public void setLift(Character lift) {
		this.lift = lift;
	}
	public Character getWashing() {
		return washing;
	}
	public void setWashing(Character washing) {
		this.washing = washing;
	}
	public Character getWaterHeater() {
		return waterHeater;
	}
	public void setWaterHeater(Character waterHeater) {
		this.waterHeater = waterHeater;
	}
	public Character getWifi() {
		return wifi;
	}
	public void setWifi(Character wifi) {
		this.wifi = wifi;
	}
	
}
