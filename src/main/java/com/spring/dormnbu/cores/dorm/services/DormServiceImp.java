package com.spring.dormnbu.cores.dorm.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DataNotFoundException;
import com.spring.dormnbu.cores.dorm.domain.Amenitie;
import com.spring.dormnbu.cores.dorm.domain.Area;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Picture;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.dorm.interfaces.DormService;
import com.spring.dormnbu.cores.entity.AmenitieEntity;
import com.spring.dormnbu.cores.entity.AreaEntity;
import com.spring.dormnbu.cores.entity.DormEntity;
import com.spring.dormnbu.cores.entity.PictureEntity;
import com.spring.dormnbu.cores.entity.PromotionEntity;
import com.spring.dormnbu.cores.entity.RoomtypeEntity;
import com.spring.dormnbu.utils.DateTimeUtil;

@Service
@Transactional
@Scope("request")
public class DormServiceImp implements DormService {

	@Autowired
	private DormDao dormDao;
	
	@Override
	public boolean checkDup(Dorm value) throws Exception {
		boolean result = false;
		
		try{
			//1. ค้นหาข้อมูล
			DormEntity data = dormDao.checkDup(value);
			
			if (data != null){
				result = true;
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Dorm add(Dorm value) throws Exception {
		Dorm result = null;
		
		try {
			//1. จัดกาข้อมูล
			DormEntity data = copyValuetoDormEntity(value);
			data.setCreateDate(DateTimeUtil.getDateCurentDate());
			data.setModify(DateTimeUtil.getDateCurentDate());
			
			for (Roomtype roomtype : value.getRoomtypes()) {
				RoomtypeEntity roomtypeEntity = new RoomtypeEntity();
				BeanUtils.copyProperties(roomtypeEntity, roomtype);
				data.addRoomtype(roomtypeEntity);
			}
			
			//2. เพิ่มข้อมูลหลัก
			data = dormDao.add(data);
			
			//3. return ข้อมุล
			result = new Dorm();
			result = copyValuetoDorm(data);
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Roomtype addRoomtype(Roomtype value) throws Exception {
		Roomtype result = null;
		
		try {
			//1. จัดกาข้อมูล
			DormEntity dorm =  dormDao.findById(value.getDormId());
			
			if (dorm != null){
				//2. set value
				RoomtypeEntity data = new RoomtypeEntity();
				data.setDorm(dorm);
				data.setType(value.getType());
				data.setFormat(value.getFormat());
				data.setSize(value.getSize());
				data.setUnit(value.getUnit());
				data.setStatus(value.getStatus());
				
				data.setMonthlyRent(value.getMonthlyRent());
				data.setMonthlyMin(value.getMonthlyMin());
				data.setMonthlyMax(value.getMonthlyMax());
				
				data.setDailyRent(value.getDailyRent());
				data.setDailyMin(value.getMonthlyMin());
				data.setDailyMax(value.getMonthlyMax());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.addRoomtype(data);
				
				//5. return ข้อมุล
				result = new Roomtype();
				result.setDormId(dorm.getDormId());
				BeanUtils.copyProperties(result, data);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Dorm edit(Dorm value) throws Exception {
		Dorm result = null;
		
		DormEntity data = dormDao.findById(value.getDormId());
		try {
			if(data != null){
				//1. จัดกาข้อมูล
				data = copyValuetoDormEntity(value);
				
				//2. เพิ่มข้อมูลหลัก
				data = dormDao.edit(data);
				
				//3. return ข้อมุล
				result = new Dorm();
				result = copyValuetoDorm(data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Amenitie editAmenitie(Amenitie value) throws Exception {
		Amenitie result = null;
		
		try {
			//1. ค้นหาข้อมูล
			AmenitieEntity data = dormDao.findAmenitieById(value.getAmenitieId());
			
			//2. จัดการผลลัพธ์
			if(data != null){
				
				//3 set value
				data.setAir(value.getAir());
				data.setCable(value.getCable());
				data.setCctv(value.getCctv());
				data.setFan(value.getFan());
				data.setFurniture(value.getFurniture());
				data.setGymnasium(value.getGymnasium());
				data.setHairSalon(value.getHairSalon());
				data.setKeycard(value.getKeycard());
				data.setLift(value.getLift());
				data.setWashing(value.getWashing());
				data.setWaterHeater(value.getWaterHeater());
				data.setWifi(value.getWifi());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.editAmenitie(data);
				
				//5. return ข้อมุล
				result = new Amenitie();
				BeanUtils.copyProperties(result, data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Area editArea(Area value) throws Exception {
		Area result = null;
		
		try {
			//1. ค้นหาข้อมูล
			AreaEntity data = dormDao.findAreaById(value.getAreaId());
			
			//2. จัดการผลลัพธ์
			if(data != null){
				
				//3 set value
				data.setNo(value.getNo());
				data.setRoad(value.getRoad());
				data.setSoi(value.getSoi());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.editArea(data);
				
				//5. return ข้อมุล
				result = new Area();
				BeanUtils.copyProperties(result, data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Picture editPicture(Picture value) throws Exception {
		Picture result = null;
		
		try {
			//1. ค้นหาข้อมูล
			PictureEntity data = dormDao.findPictureById(value.getPictureId());
			
			//2. จัดการผลลัพธ์
			if(data != null){
				
				//3 set value
				data.setPicture1(data.getPicture1());
				data.setPicture2(data.getPicture2());
				data.setPicture3(data.getPicture3());
				data.setPicture4(data.getPicture4());
				data.setPicture5(data.getPicture5());
				data.setPicture6(data.getPicture6());
				data.setPicture7(data.getPicture7());
				data.setPicture8(data.getPicture8());
				data.setPicture9(data.getPicture9());
				data.setPicture10(data.getPicture10());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.editPicture(data);
				
				//5. return ข้อมุล
				result = new Picture();
				BeanUtils.copyProperties(result, data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Promotion editPromotion(Promotion value) throws Exception {
		Promotion result = null;
		
		try {
			//1. ค้นหาข้อมูล
			PromotionEntity data = dormDao.findPromotionById(value.getPromotionId());
			
			//2. จัดการผลลัพธ์
			if(data != null){
				
				//3 set value
				data.setDetail(data.getDetail());
				data.setStartDate(data.getStartDate());
				data.setEndDate(data.getEndDate());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.editPromotion(data);
				
				//5. return ข้อมุล
				result = new Promotion();
				BeanUtils.copyProperties(result, data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Roomtype editRoomtype(Roomtype value) throws Exception {
		Roomtype result = null;
		
		try {
			//1. ค้นหาข้อมูล
			RoomtypeEntity data = dormDao.findRoomtypeById(value.getRoomtypeId());
			DormEntity dorm =  dormDao.findById(value.getDormId());
			
			//2. จัดการผลลัพธ์
			if(data != null && dorm != null){
				
				//3 set value
				data.setDorm(dorm);
				data.setType(value.getType());
				data.setFormat(value.getFormat());
				data.setSize(value.getSize());
				data.setUnit(value.getUnit());
				data.setStatus(value.getStatus());
				
				data.setMonthlyRent(value.getMonthlyRent());
				data.setMonthlyMin(value.getMonthlyMin());
				data.setMonthlyMax(value.getMonthlyMax());
				
				data.setDailyRent(value.getDailyRent());
				data.setDailyMin(value.getMonthlyMin());
				data.setDailyMax(value.getMonthlyMax());
				
				//4. เพิ่มข้อมูลหลัก
				data = dormDao.editRoomtype(data);
				
				//5. return ข้อมุล
				result = new Roomtype();
				result.setDormId(value.getDormId());
				BeanUtils.copyProperties(result, data);
			}
			else{
				throw new DataNotFoundException("key for Roomtype or Dorm - Data Not Found!");
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ค้นหาข้อมูล
			DormEntity entity = dormDao.findById(id);
			
			//2. ลบข้อมูล
			if (entity != null){
				dormDao.deleteByDormEntity(entity);
			}
			else{
				throw new DataNotFoundException("No Data for key: " + id);
			}
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public void deleteRoomtype(int id) throws Exception {
		try {
			//1. ค้นหาข้อมูล
			RoomtypeEntity entity = dormDao.findRoomtypeById(id);
			
			//2. ลบข้อมูล
			if (entity != null){
				dormDao.deleteRoomtype(entity);
			}
			else{
				throw new DataNotFoundException("No Data for key: " + id);
			}
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public Dorm findById(int id) throws Exception {
		Dorm result = null;
		
		try {
			//1. ค้นหาข้อมูล
			DormEntity data = dormDao.findById(id);
			
			//2. จัดการผลลัพธ์
			if (data != null){
				result = copyValuetoDorm(data);
			}
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public List<DormLastCreate> queryLastCreate() throws Exception {
		List<DormLastCreate> result = null;
		
		try{
			//1. ค้นหาข้อมูล
			List<DormLastCreate> datas = dormDao.queryLastCreate(DateTimeUtil.getDateCurrentMonth());
			
			//2. จัดการผลลัพธ์
			if(datas != null && !datas.isEmpty()){
				
				// create object
				result = new ArrayList<>();
				DormLastCreate dorm = null;
				
				//2.1 map data
				Map<Integer, List<DormLastCreate>> map = new HashMap<>();
				for (DormLastCreate data : datas) {
					if( map.get(data.getDormId()) == null) {
						map.put(data.getDormId(), new ArrayList<>());
					}
					map.get(data.getDormId()).add(data);
				}
				
				//2.2 loop data
				for(Map.Entry<Integer, List<DormLastCreate>> entry : map.entrySet()) {
					
					List<DormLastCreate> listData = entry.getValue();
					dorm = new DormLastCreate();
					dorm = listData.get(0);
					
					// หาค่าที่น้อยที่สุดไปแสดง
					for (DormLastCreate data : datas) {
						if (data.getMonthlyRent().equals('Y')){
							if (dorm.getMonthlyMin() > data.getMonthlyMin()){
								dorm.setMonthlyMin(data.getMonthlyMin());
								dorm.setMonthlyMax(data.getMonthlyMax());
							}
						}
						if (data.getDailyRent().equals('Y')){
							if (dorm.getDailyMin() > data.getDailyMin()){
								dorm.setDailyMin(data.getDailyMin());
								dorm.setDailyMax(data.getDailyMax());
							}
						}
					}
					result.add(dorm);
				}
				
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public List<DormPopular> queryPopular() throws Exception {
		List<DormPopular> result = null;
		
		try{
			//1. ค้นหาข้อมูล
			List<DormPopular> datas = dormDao.queryPopular();
			
			//2. จัดการผลลัพธ์
			if(datas != null && !datas.isEmpty()){
				
				// create object
				result = new ArrayList<>();
				BigDecimal zeroVal = new BigDecimal(0);
				
				// check data
				for (DormPopular data : datas) {
					if ((data.getMonthlyMin() != null && data.getMonthlyMin() != zeroVal) && (data.getMonthlyMax() != null && data.getMonthlyMax() != zeroVal) ){
						data.setMonthlyRent('Y');
					}else{
						data.setMonthlyRent('N');
						data.setMonthlyMin(new BigDecimal(0));
						data.setMonthlyMax(new BigDecimal(0));
					}
					if ((data.getDailyMin() != null && data.getDailyMin() != zeroVal) && (data.getDailyMax() != null && data.getDailyMax() != zeroVal) ){
						data.setDailyRent('Y');
					}else{
						data.setDailyRent('N');
						data.setDailyMin(new BigDecimal(0));
						data.setDailyMax(new BigDecimal(0));
					}
					result.add(data);
				}
				
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	private String genDormCode() throws Exception{
		String result = null;
		
		int minimum = 1000;
		int maximum = 9999;
		String checkDup = null;
		String code = null;
		try {
			do{
				int range = (maximum - minimum) + 1;
				int value = (int) ((Math.random() * range) + minimum);
				code = "D" + value;
				checkDup = dormDao.checkDormCode(code);
			}while(checkDup != null);
			
			result = code;
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	private DormEntity copyValuetoDormEntity(Dorm value) throws Exception{
		DormEntity result = new DormEntity();
		
		try {
			//1.1 set main value
			result.setMemberId(value.getMemberId());
			result.setDormCode(genDormCode());
			result.setNameTh(value.getNameTh());
			result.setNameEn(value.getNameEn());
			result.setPhone1(value.getPhone1());
			result.setPhone2(value.getPhone2());
			result.setEmail(value.getEmail());
			result.setLine(value.getLine());
			result.setDetail(value.getDetail());
			
			result.setWaterType(value.getWaterType());
			result.setWaterMin(value.getWaterMin());
			result.setWaterMax(value.getElectricityMax());
			result.setWaterPrice(value.getWaterPrice());
			
			result.setElectricityType(value.getElectricityType());
			result.setElectricityMin(value.getElectricityMin());
			result.setElectricityMax(value.getElectricityMax());
			result.setElectricityPrice(value.getElectricityPrice());
			
			result.setOtherServiceType(value.getOtherServiceType());
			result.setOtherServiceDesc(value.getOtherServiceDesc());
			result.setOtherServicePrice(value.getPayadvancePrice());
			
			result.setInternetType(value.getInternetType());
			result.setInternetPrice(value.getInternetPrice());
			result.setDepositType(value.getDepositType());
			result.setDepositPrice(value.getDepositPrice());
			result.setPayadvanceType(value.getPayadvanceType());
			result.setPayadvancePrice(value.getPayadvancePrice());
			
			result.setCreateDate(DateTimeUtil.stringToDate(value.getCreateDate()));
			result.setModify(DateTimeUtil.stringToDate(value.getModify()));
			
			//1.2 set area value
			Area area = value.getArea();
			AreaEntity areaEntity = new AreaEntity();
			areaEntity.setNo(area.getNo());
			areaEntity.setRoad(area.getRoad());
			areaEntity.setSoi(area.getSoi());
			result.setArea(areaEntity);
			
			//1.3 set amenitie value
			AmenitieEntity amenitieEntity = new AmenitieEntity();
			BeanUtils.copyProperties(amenitieEntity, value.getAmenitie());
			result.setAmenitie(amenitieEntity);
			
			//1.4 set picture value
			if( value.getPicture() != null ){
				PictureEntity picture = new PictureEntity();
				BeanUtils.copyProperties(picture, value.getPicture());
				result.setPicture(picture);
			}
			
			//1.4 set Promotion value
			if( value.getPromotion() != null ){
				PromotionEntity promotion = new PromotionEntity();
				promotion.setDetail(value.getPromotion().getDetail());
				promotion.setStartDate(DateTimeUtil.stringToDate(value.getPromotion().getStartDate()));
				promotion.setEndDate(DateTimeUtil.stringToDate(value.getPromotion().getEndDate()));
				result.setPromotion(promotion);
			} 
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	private Dorm copyValuetoDorm(DormEntity value) throws Exception{
		Dorm result = new Dorm();
		
		try {
			//1.1 set main value
			result.setDormId(value.getDormId());
			result.setMemberId(value.getMemberId());
			result.setDormCode(value.getDormCode());
			result.setNameTh(value.getNameTh());
			result.setNameEn(value.getNameEn());
			result.setPhone1(value.getPhone1());
			result.setPhone2(value.getPhone2());
			result.setEmail(value.getEmail());
			result.setLine(value.getLine());
			result.setDetail(value.getDetail());
			
			result.setWaterType(value.getWaterType());
			result.setWaterMin(value.getWaterMin());
			result.setWaterMax(value.getElectricityMax());
			result.setWaterPrice(value.getWaterPrice());
			
			result.setElectricityType(value.getElectricityType());
			result.setElectricityMin(value.getElectricityMin());
			result.setElectricityMax(value.getElectricityMax());
			result.setElectricityPrice(value.getElectricityPrice());
			
			result.setOtherServiceType(value.getOtherServiceType());
			result.setOtherServiceDesc(value.getOtherServiceDesc());
			result.setOtherServicePrice(value.getPayadvancePrice());
			
			result.setInternetType(value.getInternetType());
			result.setInternetPrice(value.getInternetPrice());
			result.setDepositType(value.getDepositType());
			result.setDepositPrice(value.getDepositPrice());
			result.setPayadvanceType(value.getPayadvanceType());
			result.setPayadvancePrice(value.getPayadvancePrice());
			
			result.setCreateDate(DateTimeUtil.dateToString(value.getCreateDate()));
			result.setModify(DateTimeUtil.dateToString(value.getModify()));
			
			//1.2 set area value
			Area area = new Area();
			BeanUtils.copyProperties(area, value.getArea());
			result.setArea(area);
			
			//1.3 set amenitie value
			Amenitie amenitie = new Amenitie();
			BeanUtils.copyProperties(amenitie, value.getAmenitie());
			result.setAmenitie(amenitie);
			
			//1.4 set picture value
			if( value.getPicture() != null ){
				Picture picture = new Picture();
				BeanUtils.copyProperties(picture, value.getPicture());
				result.setPicture(picture);
			}
			
			//1.5 set promotion value
			if( value.getPromotion() != null ){
				Promotion promotion = new Promotion();
				promotion.setPromotionId(value.getPromotion().getPromotionId());
				promotion.setDetail(value.getPromotion().getDetail());
				promotion.setStartDate(DateTimeUtil.dateToString(value.getPromotion().getStartDate()));
				promotion.setEndDate(DateTimeUtil.dateToString(value.getPromotion().getEndDate()));
				result.setPromotion(promotion);
			} 
			
			//1.6 set roomtype value
			List<Roomtype> roomtypes = new ArrayList<>();
			for (RoomtypeEntity roomtypeEntity : value.getRoomtypes()) {
				Roomtype roomtype = new Roomtype();
				BeanUtils.copyProperties(roomtype, roomtypeEntity);
				roomtypes.add(roomtype);
			}
			result.setRoomtypes(roomtypes);
			
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
