package com.spring.dormnbu.cores.dorm.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.error.DuplicateException;
import com.spring.dormnbu.cores.dorm.domain.Amenitie;
import com.spring.dormnbu.cores.dorm.domain.Area;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Picture;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.dorm.interfaces.DormManager;
import com.spring.dormnbu.cores.dorm.interfaces.DormService;

@Component
@Transactional
@Scope("request")
public class DormManagerImp implements DormManager {

	@Autowired
	private DormService service;

	@Override
	public Dorm add(Dorm value) throws Exception {
		Dorm result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getMemberId() != 0) {
				
				//2. ตรวจสอบข้อมูลซ้ำ
				boolean checkDup = service.checkDup(value);
				
				if (checkDup == false){
					result = service.add(value);
				}
				else{
					throw new DuplicateException("Duplicate! : This information is already in the database.");
				}
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Roomtype addRoomtype(Roomtype value) throws Exception {
		Roomtype result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null) {
				
				//2. แก้ไขข้อมูล
				result = service.addRoomtype(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Dorm edit(Dorm value) throws Exception {
		Dorm result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getDormId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.edit(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public Amenitie editAmenitie(Amenitie value) throws Exception {
		Amenitie result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getAmenitieId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.editAmenitie(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Area editArea(Area value) throws Exception {
		Area result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getAreaId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.editArea(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Picture editPicture(Picture value) throws Exception {
		Picture result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getPictureId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.editPicture(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Promotion editPromotion(Promotion value) throws Exception {
		Promotion result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getPromotionId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.editPromotion(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public Roomtype editRoomtype(Roomtype value) throws Exception {
		Roomtype result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (value != null && value.getRoomtypeId() != 0 && value.getDormId() != 0) {
				
				//2. แก้ไขข้อมูล
				result = service.editRoomtype(value);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ลบข้อมูล
				service.delete(id);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public void deleteRoomtype(int id) throws Exception {
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ลบข้อมูล
				service.deleteRoomtype(id);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Dorm findById(int id) throws Exception {
		Dorm result = null;
		
		try {
			//1. ตรวจสอบ criteria
			if (id != 0) {
				
				//2. ค้นหาข้อมูล
				result = service.findById(id);
			}
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public List<DormLastCreate> queryLastCreate() throws Exception {
		List<DormLastCreate> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			result = service.queryLastCreate();
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public List<DormPopular> queryPopular() throws Exception {
		List<DormPopular> result = null;
		
		try {
			//1. ค้นหาข้อมูล
			result = service.queryPopular();
		} catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	
	
}
