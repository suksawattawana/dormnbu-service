package com.spring.dormnbu.cores.dorm.interfaces;

import java.util.List;

import com.spring.dormnbu.cores.dorm.domain.Amenitie;
import com.spring.dormnbu.cores.dorm.domain.Area;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Picture;
import com.spring.dormnbu.cores.dorm.domain.Promotion;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;

public interface DormService {

	boolean checkDup(Dorm value) throws Exception;
	
	// Add
	Dorm add(Dorm value) throws Exception;
	
	Roomtype addRoomtype(Roomtype value) throws Exception;
	
	// Edit
	Dorm edit(Dorm value) throws Exception;
	
	Amenitie editAmenitie(Amenitie value) throws Exception;
	
	Area editArea(Area value) throws Exception;
	
	Picture editPicture(Picture value) throws Exception;
	
	Promotion editPromotion(Promotion value) throws Exception;
	
	Roomtype editRoomtype(Roomtype value) throws Exception;
	
	// Delete
	void delete(int id) throws Exception;
	
	void deleteRoomtype(int roomtypeId) throws Exception;
	
	// Query
	Dorm findById(int id) throws Exception;
	
	// Query - Where
	List<DormLastCreate> queryLastCreate() throws Exception;
	
	List<DormPopular> queryPopular() throws Exception;
}
