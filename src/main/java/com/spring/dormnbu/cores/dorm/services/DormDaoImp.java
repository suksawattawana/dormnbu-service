package com.spring.dormnbu.cores.dorm.services;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.query.internal.NativeQueryImpl;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dormnbu.commons.sql.IgnoreCaseAliasToBeanResultTransformer;
import com.spring.dormnbu.cores.dorm.domain.Dorm;
import com.spring.dormnbu.cores.dorm.domain.DormLastCreate;
import com.spring.dormnbu.cores.dorm.domain.DormPopular;
import com.spring.dormnbu.cores.dorm.domain.Roomtype;
import com.spring.dormnbu.cores.dorm.interfaces.DormDao;
import com.spring.dormnbu.cores.entity.AmenitieEntity;
import com.spring.dormnbu.cores.entity.AreaEntity;
import com.spring.dormnbu.cores.entity.DormEntity;
import com.spring.dormnbu.cores.entity.PictureEntity;
import com.spring.dormnbu.cores.entity.PromotionEntity;
import com.spring.dormnbu.cores.entity.RoomtypeEntity;

@Repository
@Transactional
@Scope("request")
public class DormDaoImp implements DormDao {

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	@Override
	public DormEntity checkDup(Dorm value) throws Exception {
		DormEntity result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Dorm.checkDup", DormEntity.class);
			query.setParameter("nameTh", value.getNameTh()); 
			query.setParameter("nameEn", value.getNameEn()); 
			query.setParameter("email", value.getEmail()); 
			query.setParameter("line", value.getLine()); 
			
			//2. query
			List<DormEntity> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String checkDormCode(String value) throws Exception {
		String result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Dorm.checkDormCode");
			query.setParameter("dormCode", value); 
			
			//2. query
			List<String> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}
	
	@Override
	public DormEntity add(DormEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public RoomtypeEntity addRoomtype(RoomtypeEntity value) throws Exception {
		try {
			em.persist(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public DormEntity edit(DormEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}
	
	@Override
	public AmenitieEntity editAmenitie(AmenitieEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public AreaEntity editArea(AreaEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public PictureEntity editPicture(PictureEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public PromotionEntity editPromotion(PromotionEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public RoomtypeEntity editRoomtype(RoomtypeEntity value) throws Exception {
		try {
			em.merge(value);
		}catch (Exception e) {
			throw e;
		}
		return value;
	}

	@Override
	public void delete(int id) throws Exception {
		try {
			//1. map sql 
			Query query = em.createNamedQuery("Dorm.delete");
			query.setParameter("dormId", id);
			
			//2. query
			query.executeUpdate();
			
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public void deleteRoomtype(RoomtypeEntity value) throws Exception {
		try {
			em.remove(value);
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public void deleteByDormEntity(DormEntity value) throws Exception {
		try {
			em.remove(value);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public DormEntity findById(int id) throws Exception {
		DormEntity result = null;
		try {
			result = em.find(DormEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Roomtype> findRoomtypeBydormId(int dormId) throws Exception {
		List<Roomtype> result = null;
		
		try{
			//1. map sql
			Query query = em.createNamedQuery("Dorm.findRoomtypeBydormId");
			query.setParameter("dormId", dormId);
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(Roomtype.class));
			
			//2. query
			result =  query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@Override
	public AmenitieEntity findAmenitieById(int id) throws Exception {
		AmenitieEntity result = null;
		try {
			result = em.find(AmenitieEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public AreaEntity findAreaById(int id) throws Exception {
		AreaEntity result = null;
		try {
			result = em.find(AreaEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public PictureEntity findPictureById(int id) throws Exception {
		PictureEntity result = null;
		try {
			result = em.find(PictureEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public PromotionEntity findPromotionById(int id) throws Exception {
		PromotionEntity result = null;
		try {
			result = em.find(PromotionEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@Override
	public RoomtypeEntity findRoomtypeById(int id) throws Exception {
		RoomtypeEntity result = null;
		try {
			result = em.find(RoomtypeEntity.class, id);
		}catch (Exception e) {
			throw e;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DormLastCreate> queryLastCreate(Date currentMonth) throws Exception {
		List<DormLastCreate> result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Dorm.queryLastCreate");
			query.setParameter("currentMonth", currentMonth);
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(DormLastCreate.class));
			
			//2. query
			result = query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DormPopular> queryPopular() throws Exception {
		List<DormPopular> result = null;
		
		try{
			//1. map sql 
			Query query = em.createNamedQuery("Dorm.queryPopular");
			query.unwrap(NativeQueryImpl.class).setResultTransformer(new IgnoreCaseAliasToBeanResultTransformer(DormPopular.class));
			
			//2. query
			result = query.getResultList();
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Integer checkNullDorm(int id) throws Exception {
		Integer result = 0;
		
		try{
			//1. map sql
			Query query = em.createNamedQuery("Dorm.checkNullDorm");
			query.setParameter("dormId", id); 
			
			//2. query
			List<Integer> listResult = query.getResultList();
			if(listResult.size() > 0 && !listResult.isEmpty()){
				result = listResult.get(0);
			}
			
		}catch (Exception e) {
			throw e;
		}
		
		return result;
	}

}
