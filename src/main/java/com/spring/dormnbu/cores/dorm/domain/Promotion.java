package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class Promotion implements Serializable {
	
	private static final long serialVersionUID = -1979668902468288263L;
	private int promotionId;
	private String detail;
	private String endDate;
	private String startDate;
	
	public int getPromotionId() {
		return promotionId;
	}
	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
}
