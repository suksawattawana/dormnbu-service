package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;
import java.math.BigDecimal;

public class DormPopular implements Serializable {
	
	private static final long serialVersionUID = 6917311088002297240L;
	private int dormId;
	private String dormCode;
	
	private String name;
	private String address;
	private String picture;
	private BigDecimal score;
	
	private Character monthlyRent;
	private BigDecimal monthlyMax;
	private BigDecimal monthlyMin;
	
	private Character dailyRent;
	private BigDecimal dailyMax;
	private BigDecimal dailyMin;
	
	public int getDormId() {
		return dormId;
	}
	public void setDormId(int dormId) {
		this.dormId = dormId;
	}
	public String getDormCode() {
		return dormCode;
	}
	public void setDormCode(String dormCode) {
		this.dormCode = dormCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public Character getMonthlyRent() {
		return monthlyRent;
	}
	public void setMonthlyRent(Character monthlyRent) {
		this.monthlyRent = monthlyRent;
	}
	public BigDecimal getMonthlyMax() {
		return monthlyMax;
	}
	public void setMonthlyMax(BigDecimal monthlyMax) {
		this.monthlyMax = monthlyMax;
	}
	public BigDecimal getMonthlyMin() {
		return monthlyMin;
	}
	public void setMonthlyMin(BigDecimal monthlyMin) {
		this.monthlyMin = monthlyMin;
	}
	public Character getDailyRent() {
		return dailyRent;
	}
	public void setDailyRent(Character dailyRent) {
		this.dailyRent = dailyRent;
	}
	public BigDecimal getDailyMax() {
		return dailyMax;
	}
	public void setDailyMax(BigDecimal dailyMax) {
		this.dailyMax = dailyMax;
	}
	public BigDecimal getDailyMin() {
		return dailyMin;
	}
	public void setDailyMin(BigDecimal dailyMin) {
		this.dailyMin = dailyMin;
	}
	
}
