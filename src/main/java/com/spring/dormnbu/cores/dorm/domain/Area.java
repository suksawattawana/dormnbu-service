package com.spring.dormnbu.cores.dorm.domain;

import java.io.Serializable;

public class Area implements Serializable {

	private static final long serialVersionUID = -4543965602772761374L;
	private int areaId;
	private String no;
	private String road;
	private String soi;
	
	public int getAreaId() {
		return areaId;
	}
	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getRoad() {
		return road;
	}
	public void setRoad(String road) {
		this.road = road;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	
	
}
